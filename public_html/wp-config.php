<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

session_start();

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'ipaye_database');

/** MySQL database username */
define('DB_USER', 'ipaye_userdata');

/** MySQL database password */
define('DB_PASSWORD', 'n3Q)s@f_I+nN');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'e~,zm#WY>Us{{i)zR3SF5*#/9&Vz9=fd3Rrch7PWU/;~GL2^<_h9!( RI{)Es0od');
define('SECURE_AUTH_KEY',  '.h[K=aMTIlj8n #u51bgD)C1KZH<EBDG[eX4U/#.I.,uWrF,.(_QnZSU.Ph!k8_t');
define('LOGGED_IN_KEY',    'Xw(rZ/RliC%1CY@qiPMUr.Lyp_B*KS[(%]8P90ZPo58>8Ky?,B9F4Uv,K^4^qAx6');
define('NONCE_KEY',        '-%tf(N;ss[=j{.!9r(z4XtC0;Z 5hMlpl&.HP_G^DhRMw*6N Mhu}ebC_ cra0*+');
define('AUTH_SALT',        't`pm|1%#=/4l{wIDM}2hce{KI`[eW~q_E[k!1 85CQ.Ee)N+%rJ$q^o89{PqTX*k');
define('SECURE_AUTH_SALT', '`Oggcu$}TaETE*i;gB=3obXh7`_2m$3mb S[5#6Duw5a[|.A(Ks]!7kz6Q(pFIGB');
define('LOGGED_IN_SALT',   '^;SBeNsJ>}o~W+G1;3(~8%z|1^9b}?(hWLeFUbZQ$x(rmb.UZ{:%@jcAW_#uQzQF');
define('NONCE_SALT',       ',Ho`#^n=2*P#xX*Rr@.HE,E2;W4U]s%~?<3`6`&_HQC(&otphaH{9A~u_09aMGGm');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);
define( 'WP_DEBUG_LOG', true );
define( 'WP_DEBUG_DISPLAY', true );
@ini_set( 'display_errors', 0 );

//define('WP_HOME','http://i-paye.peppermintdigital.com/wp');


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

//define("COOKIE_DOMAIN", "http://i-paye.peppermintdigital.com");

define("FX_SITE_URL", "http://i-paye.com");
define('WP_CONTENT_DIR', dirname(__FILE__) . '/wp-content' );
define('WP_CONTENT_URL', FX_SITE_URL . '/wp-content' );
define('WP_PLUGIN_DIR', dirname(__FILE__) . '/wp-content/plugins' );
define('WP_PLUGIN_URL', FX_SITE_URL . '/wp-content/plugins' );
//Aby obrazki byly prawidlowo dodana na;ezy zmienic nazwe katalogu
//define('UPLOADS', '../wp-content/media' );
define('UPLOADS', '../wp-content/uploads' );



$theme_root = WP_CONTENT_DIR . '/themes';

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
