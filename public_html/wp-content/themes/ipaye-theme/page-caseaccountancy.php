<?php
/*
  Template Name: Case accountancy
*/
 ?>

<?php get_header(); ?>



    <body <?php body_class(); ?>>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->


    <?php get_template_part('templates/form','panel');  ?>
    <?php get_template_part('templates/navigation','green'); ?>

       

        <header class="home-header page-header">
           

            <div class="navigation-switcher">
                <div class="switcher-wrapper">
                   <div id="switcher">
                       <span class="slice slice1"></span>
                       <span class="slice slice2"></span>
                       <span class="slice slice3"></span>
                   </div>
                </div>
            </div>
            <?php get_template_part('templates/back','accountancy'); ?>
        </header>

        <section class="main-wrapper">
          <?php get_sidebar('green'); ?>

          <div class="tiles-wrapper">
            <div class="shape-list inside-container">


            <?php 
                $paged = (get_query_var('paged')) ? get_query_var('paged') :1;
                $args = array(

                    'post_type'=>'cases_accountancy',
                    'posts_per_page'=> -1,
                    'post_status' => 'publish',
                    'paged' => $paged

                );

                $loop_case = new WP_Query($args);
              ?>


               <?php if($loop_case->have_posts() ) : ?>

                <?php while($loop_case->have_posts() ) :  $loop_case->the_post(); ?>

                <div data-case-link="<?php the_field('case_data'); ?>" class="hex-item i-paye-study">
                    <div class="inner">
                      <div class="inner-inner" style="background-image: url('<?php the_field('hex_bck'); ?>')">


                          <div class="frontface">
                              <span class="hex-btn front-btn">
                                view more
                              </span>
                          </div>

                        <div class="backface green-face">
                          <div class="inner-text">

                            <header class="back-header-center">
                                <h3 class="hex-title"><?php the_field('author_name'); ?></h3>
                                <h4 class="hex-sub-title"><?php the_field('author_title'); ?></h4>
                            </header>

                           <p><?php the_excerpt_max_charlength(120); ?></p>

                            <span class="hex-btn back-btn">
                              view more
                            </span>

                          </div>
                        </div>
                      </div>
                    </div>
                </div>

                <?php endwhile; wp_reset_query(); ?>

            <?php endif; ?>

            </div>
          </div>




        <div class="content-inner">

            <?php 
                $paged = (get_query_var('paged')) ? get_query_var('paged') :1;
                $args = array(

                    'post_type'=>'cases_accountancy',
                    'posts_per_page'=> -1,
                    'post_status' => 'publish',
                    'paged' => $paged

                );

                $loop_case = new WP_Query($args);
              ?>

              <?php if($loop_case->have_posts() ) : ?>

                <?php while($loop_case->have_posts() ) :  $loop_case->the_post(); ?>

                  <article data-case-link="<?php the_field('case_data'); ?>" class="topics">
                    <div class="topic-content-wrapper green-content-inner">

                        <header class="topic-header">
                            <div class="close-panel"></div>

                            <div class="arrows-wrapper clearfix">

                              <a href="#" class="prev">Prev</a>
                              <a href="#" class="next">Next</a>
                      
                              </div>
                        </header>

                          <div class="article-show">
                            <div class="case-description">

                           <?php 
                                if(has_post_thumbnail()) {
                                    the_post_thumbnail('full', array('alt'=>get_the_title(), 'class'=>'case-avatar')); 
                                }
                          ?>

                            

                              <header class="desc-header">
                                <h3 class="case-study-header"><?php the_field('author_name'); ?></h3>
                                <h4 class="job-title-case"><?php the_field('author_title'); ?></h4>
                              </header>

                                <?php the_content(); ?>
                            </div>

                            </div>
                    </div>

                  </article> 

                  <?php endwhile; wp_reset_query(); ?>

          <?php endif; ?>

          </div>


        <?php get_template_part('templates/footer','hexagonacc'); ?>
        </section>


<?php get_footer(); ?>


    </body>
</html>
