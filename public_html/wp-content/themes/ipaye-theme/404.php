

<?php get_header(); ?>


    <body <?php body_class(); ?>>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->


        <section class="main-wrapper">


         <div class="page-404-wrapper">
              <div class="page-inner page-inner-full-width">
                  

            <section class="page-content clearfix">

                <section class="main-page-content">

                <h1 class="main-title-panel-left purple-header-left">404</h1>

                  <h3 class="content-title-sub orange-sub">
                    This is not the web page you are looking for.
                  </h3>

              <a class="cta-btn orange-btn" href="<?php echo esc_url(home_url('/')); ?>">Back Home</a>

                </section>
                      

                      <section class="page-contact-area">
                        <h3 class="content-title-main grey-header">For any enquiries please fill in the form below. </h3>

                        <div class="page-contant-form-wrapper">

                          <?php echo do_shortcode('[contact-form-7 id="118" title="Main contact form" html_id="page-contact-form" html_class="contact-form page-form-ipaye"]') ?>
                        </div>
                      </section>


                  </section>

              </div>
          </div>


          
          <?php get_template_part('templates/footer','bottom'); ?>
            
        </section>


        <section id="download-form-wrapper" class="mfp-hide modal-box download-pop">
            <section class="download-section">
              <h3 class="download-head"><span>To receive password,</span> <span>please fill the form below</span></h3>
              <?php echo do_shortcode('[contact-form-7 id="575" html_class="contact-form page-form-ipaye" title="Download form"]') ?>
            </section>
         </section>



<?php get_footer(); ?>


    </body>
</html>
