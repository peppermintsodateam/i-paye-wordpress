<?php
/*
  Template Name: Page features i-paye
*/
 ?>


 <?php get_header(); ?>


    <body <?php body_class(); ?>>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

      <?php get_template_part('templates/form','panel');  ?>
     


      <?php

            if(is_page(370)) {
                get_template_part('templates/navigation','main');
            } else if(is_page(422)) {
                 get_template_part('templates/navigation','green');
            } else if(is_page(427)) {
                get_template_part('templates/navigation','orange');
            }

          ?>

        <header class="home-header page-header">
           

            <div class="navigation-switcher">
                <div class="switcher-wrapper">
                   <div id="switcher">
                       <span class="slice slice1"></span>
                       <span class="slice slice2"></span>
                       <span class="slice slice3"></span>
                   </div>
                </div>
            </div>


             <?php

                if(is_page(370)) {
                  
                    get_template_part('templates/back','ipaye');

                } else if(is_page(422)) {
                   get_template_part('templates/back','accountancy');
                } else if(is_page(427)) {
                    get_template_part('templates/back','spi');
                }

            ?>

            
        </header>

        <section class="main-wrapper">

          

        <?php

            if(is_page(370)) {
              get_sidebar('purple');
            } else if(is_page(422)) {
                get_sidebar('green');
            } else if(is_page(427)) {
                get_sidebar('orange');
            }

        ?>

          <div class="tiles-wrapper">

            <div class="flip-tiles-wrapper inside-container">
              
              <?php

                if(is_page(370)) {
                  get_template_part('templates/features','ipaye');
                } else if(is_page(422)) {
                   get_template_part('templates/features','accountancy');
                } else if(is_page(427)) {
                    get_template_part('templates/features','spi');
                }

              ?>



              <?php 
                 if(is_page(370)) {
                    get_template_part('templates/loop','featureipaye');
                 }else if(is_page(422)) {
                    get_template_part('templates/loop','featureacc');
                 }else if(is_page(427)) {
                    get_template_part('templates/loop','featurespi');
                 }
              ?>
              

            </div>
          </div>


            <?php 
                 if(is_page(370)) {
                    get_template_part('templates/footer','grid');
                 }else if(is_page(422)) {
                    get_template_part('templates/footer','gridacc');
                 }else if(is_page(427)) {
                    get_template_part('templates/footer','gridspi');
                 }
              ?>

        </section>

       


    <?php get_footer(); ?>


    </body>
</html>
