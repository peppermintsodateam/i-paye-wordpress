<?php 
    $short_content = get_field('short_content');
?>

<aside class="left-panel-wrapper i-paye-left-panel">
            <div class="bck-top"></div>

            <div class="left-content-scroll">
              <div class="panel-content">
                <div class="panel-content-inside">
                    <figure class="logo-icon-panel">
                        <img class="panel-logo" src="<?php echo THEME_PATH; ?>/gfx/spi-accountancy-logo.svg" alt="Spi Logo">
                    </figure>

                    <div class="content-panel-text">
                        <h1 class="main-title-panel-left orange-header-left"><?php the_title(); ?></h1>
                        <?php echo $short_content; ?>
                    </div>

                    <div class="panel-contact-details orange-details">
                        <?php if(is_active_sidebar('orange-widget')) : ?>

                            <?php dynamic_sidebar('orange-widget'); ?>

                        <?php endif; ?>
                    </div>

                    <?php get_template_part('templates/spanel','orange'); ?>

                </div>
              </div>
            </div>

            <div class="bck-bottom-purple"></div>
</aside>