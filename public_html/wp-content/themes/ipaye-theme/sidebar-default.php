<?php 
    $short_content = get_field('short_content');
?>

<aside class="left-panel-wrapper i-paye-left-panel">
            <div class="bck-top"></div>

            <div class="left-content-scroll">
              <div class="panel-content">
                <div class="panel-content-inside">
                    <figure class="logo-icon-panel">
                        <img class="panel-logo" src="<?php echo THEME_PATH; ?>/gfx/logo-set.svg" alt="I-paye, I-paye accountancy, I-paye SPI">
                    </figure>

                    <div class="content-panel-text">
                        <h1 class="main-title-panel-left purple-header-left"><?php the_title(); ?></h1>
                        <?php echo $short_content; ?>
                    </div>

                    <div class="panel-contact-details purple-details">
                        <?php if(is_active_sidebar('general-sidebar')) : ?>

                            <?php dynamic_sidebar('general-sidebar'); ?>

                        <?php endif; ?>
                    </div>


                </div>
              </div>
            </div>

            <div class="bck-bottom"></div>
</aside>