<?php
/*
	Template Name: Page countries
*/
 ?>

<?php get_header(); ?>


    <body <?php body_class(); ?>>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

      <?php get_template_part('templates/form','panel');  ?>

    	


        <?php get_template_part('templates/navigation','main'); ?>


        <header class="home-header page-header">
           

            <div class="navigation-switcher">
                <div class="switcher-wrapper">
                   <div id="switcher">
                       <span class="slice slice1"></span>
                       <span class="slice slice2"></span>
                       <span class="slice slice3"></span>
                   </div>
                </div>
            </div>
            <a href="<?php echo esc_url(home_url('/') ); ?>" class="back-btn back-home">Home</a>
        </header>

        <section class="main-wrapper">

          <?php get_sidebar('purple'); ?>

         <div class="page-container">
              <div class="page-inner">
                  <section class="page-inner-header">
                      <div class="col-head logo-col flip-container-shape">
                         <div class="flipper-shape">
                          <?php get_template_part('templates/content','header'); ?>
                             <div class="back-shape shape-bck"></div>  
                         </div>
                      </div>

                      <div class="col-head desc-col">
                          <div class="description-inner">
                              <div class="inner-text">
                              <h3 class="top-desc-page"><?php the_field('top_short_content'); ?></h3>
                              </div>
                          </div>
                      </div>
                  </section>

            <section class="page-content clearfix">

                <section class="main-page-content">
                  <div class="download-guide-wrapper">
                    <a data="download-section" href="#" class="download-button move-purple"><?php echo get_field('button_download_text'); ?></a>
                  </div>

                             <?php if(have_posts() ) : ?>

                                <?php while(have_posts() ) : the_post(); ?>

                                    <?php the_content(); ?>

                                <?php  endwhile; ?>

                            <?php endif; wp_reset_query(); ?>


                        <section data="download-section" id="download-section" class="download-section-wrapper">
                        <?php
                          if(is_page(550)) {
                            get_template_part('loops/loop','documents');
                          }else if(is_page(538)) {
                            get_template_part('loops/loop','download');
                          }
                         ?>
                            
                        </section>

                    </section>
                      

                      <section class="page-contact-area">
                        <h3 class="content-title-main grey-header">For any enquiries please fill in the form below. </h3>

                        <div class="page-contant-form-wrapper">

                          <?php echo do_shortcode('[contact-form-7 id="118" title="Main contact form" html_id="page-contact-form" html_class="contact-form page-form-ipaye"]') ?>
                        </div>
                      </section>


                  </section>

              </div>
          </div>


          
          <?php get_template_part('templates/footer','bottom'); ?>
            
        </section>


        <section id="download-form-wrapper" class="mfp-hide modal-box download-pop">
            <section class="download-section">
              <h3 class="download-head"><span>To receive password,</span> <span>please fill the form below</span></h3>
              <?php echo do_shortcode('[contact-form-7 id="575" html_class="contact-form page-form-ipaye" title="Download form"]') ?>
            </section>
         </section>



<?php get_footer(); ?>


    </body>
</html>
