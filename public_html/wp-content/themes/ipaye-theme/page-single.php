<?php
/*
	Template Name: Page layout
*/
 ?>

<?php get_header(); ?>


    <body <?php body_class(); ?>>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

      <?php get_template_part('templates/form','panel');  ?>
    	<?php get_template_part('templates/navigation','main'); ?>


        <header class="home-header page-header">
           

            <div class="navigation-switcher">
                <div class="switcher-wrapper">
                   <div id="switcher">
                       <span class="slice slice1"></span>
                       <span class="slice slice2"></span>
                       <span class="slice slice3"></span>
                   </div>
                </div>
            </div>
            <a href="<?php echo esc_url(home_url('/') ); ?>" class="back-btn back-home">Home</a>
        </header>

        <section class="main-wrapper">

          <?php get_sidebar('default'); ?>

          <div class="page-container single-page">

                <div class="page-inner single-page">
                      
                  <section class="page-content clearfix">

                        <section class="main-page-content">
                          <?php if(is_page(700)) {
                              get_template_part('templates/template','agency');
                            }else 
                              get_template_part('loops/loop','general')
                           
                          ?>
                          
                          <?php get_template_part('templates/page','socials'); ?>
                        </section>

                        <section class="page-bottom-area">
<h4 class="content-title-sub orange-sub"><?php the_field('call_paragraph'); ?> <a class="panel-contact-number" href="<?php the_field('phone_link'); ?>"><?php the_field('phone_text'); ?></a> </h4>
                        </section>


                  </section>
                    
                </div>
          </div>


          
          <?php get_template_part('templates/footer','bottom'); ?>
            
        </section>

 
<?php get_footer(); ?>


    </body>
</html>
