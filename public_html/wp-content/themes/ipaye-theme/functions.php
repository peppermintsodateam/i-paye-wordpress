<?php

/*--------------------------------
        THEME CONFIGURATION
--------------------------------*/
define('THEME_PATH', get_template_directory_uri());

/*update_option( 'siteurl', 'http://i-paye.com/wp' );*/

/*--------------------------------
   DEFINE PATH (DIFFERENT METHOD)
--------------------------------*/
if (!defined('IPAYE_THEME_DIR')) {
    define('IPAYE_THEME_DIR', get_theme_root() . '/' . get_template() . '/');
}

if (!defined('IPAYE_THEME_DIR')) {
    define('IPAYE_THEME_DIR', WP_CONTENT_URL . '/themes/' . get_template() . '/');
}

require_once IPAYE_THEME_DIR . 'libs/posttypes.php';

/*--------------------------------
        LOAD JQUERY
--------------------------------*/
function jquery_scripts_method()
{
    wp_enqueue_script('jquery');
}

add_action('wp_enqueue_scripts', 'jquery_scripts_method');

// Defer Javascripts
// Defer jQuery Parsing using the HTML5 defer property
if (!(is_admin())) {
    function defer_parsing_of_js($url)
    {
        if (false === strpos($url, '.js')) {
            return $url;
        }
        if (strpos($url, 'jquery.js')) {
            return $url;
        }

        // return "$url' defer ";
        return "$url' defer onload='";
    }

    add_filter('clean_url', 'defer_parsing_of_js', 11, 1);
}


/*--------------------------------
    CUSTOM JQUERY PLUGINS
--------------------------------*/

function plugins_enqueue()
{
    wp_register_script('tween_max',
        'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.0/TweenMax.min.js', ['jquery'],
        "", false);
    wp_register_script('scroll_magic',
        'http://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js',
        ['jquery'], "", false);
    wp_register_script('scroll_debugger',
        'http://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.min.js',
        ['jquery'], "", false);
    wp_register_style('font_awesome',
        'http://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css', [],
        null, 'all');

    wp_register_script('modernizr',
        get_template_directory_uri() . '/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js',
        ['jquery'], "", false);
    wp_register_script('gsap',
        get_template_directory_uri() . '/js/vendor/animation.gsap.min.js', ['jquery'],
        "1.0", true);
    wp_register_script('canvas_js',
        get_template_directory_uri() . '/js/vendor/circle-canvas.js', ['jquery'], "1.0",
        true);
    wp_register_script('mixup',
        get_template_directory_uri() . '/js/vendor/mixitup.min.js', ['jquery'], "1.0",
        true);
    wp_register_script('ajax_js',
        get_template_directory_uri() . '/js/vendor/ajax-script.js', ['jquery'], "1.1",
        true);
    wp_register_script('owl_carousel',
        get_template_directory_uri() . '/js/vendor/owl.carousel.min.js', ['jquery'],
        "1.0", true);
    wp_register_script('hammer_time',
        get_template_directory_uri() . '/js/vendor/hammer-time.js', ['jquery'], "1.0",
        true);
    wp_register_script('hammer', get_template_directory_uri() . '/js/vendor/hammer.js',
        ['jquery'], "1.0", true);

    wp_register_script('map', get_template_directory_uri() . '/js/vendor/google-map.js',
        ['jquery'], "1.0", true);

    wp_register_script('match_height',
        get_template_directory_uri() . '/js/vendor/jquery.matchHeight-min.js', ['jquery'],
        "1.0", true);
    wp_register_script('pop-up',
        get_template_directory_uri() . '/js/vendor/jquery.magnific-popup.min.js',
        ['jquery'], "1.0", true);
    wp_register_script('bezier',
        get_template_directory_uri() . '/js/vendor/jquery.bez.js', ['jquery'], "1.0",
        true);
    wp_register_script('main_js', get_template_directory_uri() . '/js/main.js',
        ['jquery'], "1.0", true);

    wp_register_style('screen', get_template_directory_uri() . '/css/screen.css', [],
        null, 'all');
    wp_register_style('main', get_template_directory_uri() . '/css/main.css', [], null,
        'all');

    wp_enqueue_script('tween_max');
    wp_enqueue_script('scroll_magic');
    wp_enqueue_script('scroll_debugger');


    wp_enqueue_script('modernizr');
    wp_enqueue_script('gsap');

    if (is_front_page()) {
        wp_enqueue_script('canvas_js');
    }


    if (is_page(352)) {
        wp_enqueue_script('map');
    }

    if (is_page(9)) {
        wp_enqueue_script('mixup');
        wp_enqueue_script('ajax_js');
    }

    /* if(is_page(293)) {
       wp_enqueue_script('mixup');
    }*/

    wp_enqueue_script('owl_carousel');
    wp_enqueue_script('hammer_time');
    wp_enqueue_script('hammer');
    wp_enqueue_script('match_height');
    wp_enqueue_script('pop-up');
    wp_enqueue_script('bezier');
    wp_enqueue_script('mixup');
    wp_enqueue_script('main_js');

    wp_enqueue_style('font_awesome');
    wp_enqueue_style('screen');
    wp_enqueue_style('main');
}


add_action('wp_enqueue_scripts', 'plugins_enqueue');

/*--------------------------------
        REMOVE ADMIN BAR
--------------------------------*/
add_filter('show_admin_bar', '__return_false');


/*--------------------------------
        <p> AND <br> TAGS SPACES
--------------------------------*/
function remove_empty_p($content)
{
    $content = force_balance_tags($content);

    return preg_replace('#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content);
}

add_filter('the_content', 'remove_empty_p', 20, 1);


remove_filter('the_sidebar', 'wpautop');

remove_filter('the_excerpt', 'wpautop');

/*--------------------------------
 FEATURED IMAGES IN DASHBOARD SUPPORT
--------------------------------*/

function check_thumbnail_support()
{
    if (!current_theme_supports('post-thumbnails')) {
        add_theme_support('post-thumbnails');
        //add_action('init','remove_posttype_thumbnail_support');
    }
}

add_action('after_setup_theme', 'check_thumbnail_support');


$_SERVER['HTTPS'] = false;

function get_ajax_page()
{

    header("Content-Type: application/json");
    $id_post = $_POST['post_id'];
    $post_att = get_post($id_post);

    $url_post = get_permalink($id_post);
    $post_att->the_permalink = $url_post;

    echo json_encode($post_att, JSON_UNESCAPED_UNICODE);
}

add_action('wp_ajax_nopriv_get-content-page', 'get_ajax_page');
add_action('wp_ajax_get-content-page', 'get_ajax_page');

/*Titles suppport*/
add_theme_support('title-tag');

/*--------------------------------
    REGISTER NAVIGATIONS
--------------------------------*/


if (function_exists('ipaye_menu')) {

    register_nav_menus(

        [
            'main-menu' => __('Main Menu'),
            'footer-menu' => __('Footer Menu'),
        ]

    );
}


/*--------------------------------
 IMAGE SIZE CUSTOM FUNCTION
--------------------------------*/
add_image_size('custom-image', 1920, 1080, true);
add_image_size('team-image', 250, 250, true);
add_image_size('footer-image', 9999, 110, true);


/*--------------------------------
 EXCERPT FUNCTION
--------------------------------*/

function the_excerpt_max_charlength($charlength)
{
    echo cutText(get_the_excerpt(), $charlength);
}

function cutText($text, $maxLength)
{

    $maxLength++;

    $return = '';
    if (mb_strlen($text) > $maxLength) {
        $subex = mb_substr($text, 0, $maxLength - 5);
        $exwords = explode(' ', $subex);
        $excut = -(mb_strlen($exwords[count($exwords) - 1]));
        if ($excut < 0) {
            $return = mb_substr($subex, 0, $excut);
        } else {
            $return = $subex;
        }
        $return .= '[...]';
    } else {
        $return = $text;
    }

    return $return;
}


/*function show_btn($args) {
  $show_btn = file_get_contents(IPAYE_THEME_DIR."/btn.php");
  return do_shortcode($show_btn);
}*/

function show_btn()
{

    if (function_exists('have_rows')) {
        ob_start();

        if (have_rows('button_back')) :

            echo "<div class='btn-back-wrapper'>"

            ?>

            <?php while (have_rows('button_back')) :the_row(); ?>
            <a href="<?php the_sub_field('button_link'); ?>"
               class="move-home <?php the_sub_field('button_class'); ?>"><?php the_sub_field('button_text'); ?></a>
        <?php endwhile; ?>

            </div>

        <?php endif;

        $content = ob_get_contents();
        ob_end_clean();

        return $content;
    }

}

add_shortcode('show_btn', 'show_btn');

/*--------------------------------
 SIDEBARS
--------------------------------*/

if (function_exists('register_sidebar')) {
    $sidebar_list = [
        [
            'name' => 'Main sidebar',
            'id' => 'general-sidebar',
            'description' => 'general-widget-area',
        ],
        [
            'name' => 'Purple',
            'id' => 'purple-widget',
            'description' => 'This is purple widget area',
        ],

        [
            'name' => 'Orange',
            'id' => 'orange-widget',
            'description' => 'This is orange widget area',
        ],
        [
            'name' => 'Green',
            'id' => 'green-widget',
            'description' => 'This is green widget area',
        ],
        [
            'name' => 'Contact sidebar',
            'id' => 'contact-widget',
            'description' => 'This is contact widget area',
        ],
        [
            'name' => 'Assistance sidebar',
            'id' => 'assistance-widget',
            'description' => 'This is assistance widget area',
        ],
        [
            'name' => 'Common sidebar',
            'id' => 'common-widget',
            'description' => 'This is common widget area',
        ],
        [
            'name' => 'Team sidebar',
            'id' => 'team-sidebar',
            'description' => 'Team-sidebar-area',
        ],
    ];

    $sidebar_opts = [
        'before_widget' => '<div id="%1$s" class="sidebar-box %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ];

    foreach ($sidebar_list as $sidebar) {
        register_sidebar(array_merge($sidebar, $sidebar_opts));
    }

}

//Remove default widget title
add_filter('widget_title', 'my_widget_title');
function my_widget_title($t)
{
    return null;
}


add_filter('genesis_post_title_output', 'remove_single_custom_post_titles');

function remove_single_custom_post_titles($title)
{
    $single_titles = '';

    $archive_titles = $title;

    $title = is_singular('sdm_downloads') ? $single_titles : $archive_titles;

    return $title;
}

//Allow to attach SVG to Wordpress library
function cc_mime_types($mimes)
{
    $mimes['svg'] = 'image/svg+xml';

    return $mimes;
}

add_filter('upload_mimes', 'cc_mime_types');

/*BREADCRUMBS*/

function qt_custom_breadcrumbs()
{

    $showOnHome = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
    $delimiter = '&raquo;'; // delimiter between crumbs
    $home = 'Home'; // text for the 'Home' link
    $showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
    $before = '<span class="current">'; // tag before the current crumb
    $after = '</span>'; // tag after the current crumb

    global $post;
    $homeLink = get_bloginfo('url');

    if (is_home() || is_front_page()) {

        if ($showOnHome == 1) {
            echo '<div id="crumbs"><a href="' . $homeLink . '">' . $home . '</a></div>';
        }

    } else {

        echo '<div id="crumbs"><a href="' . $homeLink . '">' . $home . '</a> '
            . $delimiter . ' ';

        if (is_category()) {
            $thisCat = get_category(get_query_var('cat'), false);
            if ($thisCat->parent != 0) {
                echo get_category_parents($thisCat->parent, true, ' ' . $delimiter . ' ');
            }
            echo $before . 'Archive by category "' . single_cat_title('', false) . '"'
                . $after;

        } else if (is_search()) {
            echo $before . 'Search results for "' . get_search_query() . '"' . $after;

        } else if (is_day()) {
            echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y')
                . '</a> ' . $delimiter . ' ';
            echo '<a href="' . get_month_link(get_the_time('Y'), get_the_time('m')) . '">'
                . get_the_time('F') . '</a> ' . $delimiter . ' ';
            echo $before . get_the_time('d') . $after;

        } else if (is_month()) {
            echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y')
                . '</a> ' . $delimiter . ' ';
            echo $before . get_the_time('F') . $after;

        } else if (is_year()) {
            echo $before . get_the_time('Y') . $after;

        } else if (is_single() && !is_attachment()) {
            if (get_post_type() != 'post') {
                $post_type = get_post_type_object(get_post_type());
                $slug = $post_type->rewrite;
                echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/">'
                    . $post_type->labels->singular_name . '</a>';
                if ($showCurrent == 1) {
                    echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
                }

            } else {
                $cat = get_the_category();
                $cat = $cat[0];
                $cats = get_category_parents($cat, true, ' ' . $delimiter . ' ');
                if ($showCurrent == 0) {
                    $cats = preg_replace("#^(.+)\s$delimiter\s$#", "$1", $cats);
                }
                echo $cats;
                if ($showCurrent == 1) {
                    echo $before . get_the_title() . $after;
                }
            }

        } else if (!is_single() && !is_page() && get_post_type() != 'post' && !is_404()) {
            $post_type = get_post_type_object(get_post_type());
            echo $before . $post_type->labels->singular_name . $after;

        } else if (is_attachment()) {
            $parent = get_post($post->post_parent);
            $cat = get_the_category($parent->ID);
            $cat = $cat[0];
            echo get_category_parents($cat, true, ' ' . $delimiter . ' ');
            echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title
                . '</a>';
            if ($showCurrent == 1) {
                echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
            }

        } else if (is_page() && !$post->post_parent) {
            if ($showCurrent == 1) {
                echo $before . get_the_title() . $after;
            }

        } else if (is_page() && $post->post_parent) {
            $parent_id = $post->post_parent;
            $breadcrumbs = [];
            while ($parent_id) {
                $page = get_page($parent_id);
                $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">'
                    . get_the_title($page->ID) . '</a>';
                $parent_id = $page->post_parent;
            }
            $breadcrumbs = array_reverse($breadcrumbs);
            for ($i = 0; $i < count($breadcrumbs); $i++) {
                echo $breadcrumbs[$i];
                if ($i != count($breadcrumbs) - 1) {
                    echo ' ' . $delimiter . ' ';
                }
            }
            if ($showCurrent == 1) {
                echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
            }

        } else if (is_tag()) {
            echo $before . 'Posts tagged "' . single_tag_title('', false) . '"' . $after;

        } else if (is_author()) {
            global $author;
            $userdata = get_userdata($author);
            echo $before . 'Articles posted by ' . $userdata->display_name . $after;

        } else if (is_404()) {
            echo $before . 'Error 404' . $after;
        }

        if (get_query_var('paged')) {
            if (is_category() || is_day() || is_month() || is_year() || is_search()
                || is_tag()
                || is_author()
            ) {
                echo ' (';
            }
            echo __('Page') . ' ' . get_query_var('paged');
            if (is_category() || is_day() || is_month() || is_year() || is_search()
                || is_tag()
                || is_author()
            ) {
                echo ')';
            }
        }

        echo '</div>';

    }
} // end qt_custom_breadcrumbs()


// define the wpcf7_is_tel callback
function custom_filter_wpcf7_is_tel($result, $tel)
{
    $result = preg_match('/^\(?\+?([0-9]{1,4})?\)?[-\. ]?(\d{10})$/', $tel);

    return $result;
}

add_filter('wpcf7_is_tel', 'custom_filter_wpcf7_is_tel', 10, 2);


/*--------------------------------
  CLASSES TO PREV NEXT BUTTON
--------------------------------*/

add_filter('next_post_link', 'post_link_attributes_next');
add_filter('previous_post_link', 'post_link_attributes_prev');

function post_link_attributes_next($output)
{
    $code = 'class="next-article right-btn"';

    return str_replace('<a href=', '<a ' . $code . ' href=', $output);
}

function post_link_attributes_prev($output)
{
    $code = 'class="prev-article left-btn"';

    return str_replace('<a href=', '<a ' . $code . ' href=', $output);
}


if (!function_exists('custom_post_nav')) :
    /**
     * Display navigation to next/previous post when applicable.
     */
    function custom_post_nav()
    {
        // Don't print empty markup if there's nowhere to navigate.
        $previous = (is_attachment()) ? get_post(get_post()->post_parent)
            : get_adjacent_post(false, '', true);
        $next = get_adjacent_post(false, '', false);

        if (!$next && !$previous) {
            return;
        }
        ?>
        <nav class="navigation post-navigation" role="navigation">
            <div class="nav-links">
                <?php
                previous_post_link('<div class="nav-previous">%link</div>',
                    _x('Previous', 'acrylic'));
                next_post_link('<div class="nav-next">%link</div>',
                    _x('Next', 'acrylic'));
                ?>
            </div>
        </nav>
        <?php
    }

endif;
