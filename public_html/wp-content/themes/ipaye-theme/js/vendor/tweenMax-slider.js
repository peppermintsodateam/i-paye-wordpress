(function($){

    // VARIABLES
    var navToggle = $('.nav-toggle > .open');
    var navToggleClose = $('.nav-toggle > .close');
    var headerHeight = $('#main-header').outerHeight();

    var sectionFull = $('.section-height');
    var sectionAbout = $('.about-bck-section');
    var header = $('#main-header');

    // VARIABLES FOR SLIDER
    var mainContent = $('.main-wrapper-studies');
    var slidesContainer = mainContent.find('.slides-container');
    var slides = slidesContainer.children();
    var curSlide = slides.first();
    var slidesCount = slides.length;
    var sections = $('.section');
    var linksPag = $('.main-pagination li').find('a');
    var navPrev = $('.buttons-wrapper').find('.left-btn');
    var navNext = $('.buttons-wrapper').find('.right-btn');
    var pagination = $('.main-pagination');
    var currentSlideID = 0;
    var loadedPage = false;
    var isAnimating = false;
    var refresh = false;
    var mainNavigationWrapper = $('.main-nav-wrapper').find('.nav-container');

    $.fn.setNavHeight = function() {

        return this.css('height', $(window).height());

    };

    
    function containerWidth() {
        //slidesContainer.width($(window).width()*4);

        slidesContainer.css({
            'width': (slidesCount*100+'%')
        });

        slides.width($(window).width());
    }

    navPrev.on("click", gotoPrevSlide);
    navNext.on("click", gotoNextSlide);

    if($('.section').length===1) {
        $('.buttons-wrapper').find('.right-btn').hide();
    }else {
        $('.buttons-wrapper').find('.right-btn').show();
    }

    function gotoPrevSlide() {

        var slideToGo = currentSlideID - 1;
        if (slideToGo <= -1) {
            slideToGo = slidesCount - 1;
        }
        gotoSlide(slideToGo,false);

    }

    function gotoNextSlide() {

        var slideToGo = currentSlideID + 1;
        if (slideToGo >= slidesCount) {
            slideToGo = 0;
        }
        gotoSlide(slideToGo,false);
    }

    function gotoSlide(slideID,loadedPage) {
        loadedPage = loadedPage;
        currentSlideID = slideID;
        linksPag.eq(slideID).click();
    }

    function setHeightMedia() {
        setTimeout(function(){

            $('.slider-content').matchHeight({ property: 'min-height' });
        },280);
    }



    // SECTION HEIGHT
    $.fn.sectionHeight = function() {
        return this.each(function(){
            var that = $(this);
            that.css('min-height', $(window).height() - $('.footer-wrapper').outerHeight(true) - $('#main-header').outerHeight(true) - $('.case-study-header').outerHeight(true));

        });
    }



    scrollToSection = function(e) {
        
            e.preventDefault();
            var that = $(this);
            var targetId = that.attr('href');
            if(!targetId) return;

            var target = $(targetId);
            var offset = target.position().left;
            var matchTarget = targetId.split('-');

            currentSlideID = Number(matchTarget[matchTarget.length -1]) - 1;

            navPrev.off().on("click", gotoPrevSlide).removeClass('disabled');
            navNext.off().on("click", gotoNextSlide).removeClass('disabled');

                if(currentSlideID===0) {
                    // Pierwszy slajd
                    // Off odpina handlery
                    navPrev.off().addClass('disabled');

                    //Jestem na ostatnim slajdzie
                }else if (currentSlideID===slidesCount - 1) {
                    navNext.off().addClass('disabled');
                }

            // jesli nie on resize to animuj inaczej ustaw odrazu w
            // odpowiedniej pozycji
            if (!refresh) {


                var time = (loadedPage) ? 0 : 0.5;
                loadedPage = false;
                
                TweenMax.to($('.slides-container'),time, {
                    marginLeft: -offset,
                    onComplete: function () {
                        isAnimating = false;

                        var currentContainer = slides.eq(currentSlideID);
                        //tutaj mozesz sobie odpalic jakas animacje wewnatrz
                        // np tylko dla drugiego kontenera
                        TweenMax.to(currentContainer.find('.left-content'), 0.4, {
                                left: 0,
                                delay:0.5,
                                ease: Power4.easeOut,
                                onComplete:function() {

                                    TweenMax.to(currentContainer.find('.right-content'), 0.4, {
                                        right: 0,
                                        onComplete:function() {
                                            TweenMax.to($('.section-caption'), 1.0, 
                                                    {
                                                        delay:0.2,
                                                        opacity:1,
                                                        bottom:"25%" ,
                                                        ease: Power2.easeOut
                                                    }

                                                )
                                        }
                                    }

                            )   }
                        })

                    },
                    ease: Power1.easeOut
                });


                /*autoAlpha:0,
                            y:'+=150',
                            ease:Back.easeOut*/
        

            } else {
                $('.slides-container').css('margin-left', -offset);
            }

            refresh = false;

            if (typeof history.pushState !== "undefined") {
                var id = target.attr('id');
                history.pushState({id: id}, '', "#" + id);
             }

        }


    function findHash() {

        loadedPage = true;

        $(linksPag).filter(function(){
        return $(this).attr("href")===window.location.hash;
        }).trigger("click");

    }

    function switchNavigation() {

        var nav = $('.main-nav-wrapper');
        var background = nav.find('.bck');
        var container = nav.find('.nav-container');
        var triangle = $('.triangle-nav');
        var items = nav.find('.menu > .menu-elem');
        var openMe = nav.find('.open');
        var openLine = openMe.find('.slice');
        var closeMe = nav.find('.close');
        var closeLines = closeMe.find('.slice');

        var wait = nav.data('wait');

        if (wait)
            return;
        nav.data('wait', true);


        if(nav.hasClass('is-opened')) {
            openMe.css({
                 opacity: 1
            });

            openLine.css({
                opacity: 0
            });

            var tl = new TimelineMax();
            tl.pause();
            tl.fromTo(background,1.5, {
                alpha:1,
                x: 0,
                skewX : 0,
                scaleY: 1
            }, {
                alpha: 1,
                x: -$(window).width() + triangle.width(),
                skewX: -45,
                scaleY: 2,
                ease: Power3.easeInOut
            },0);

            tl.staggerFromTo(items,0.6, {
                alpha:1,
                x:0
            }, {

                 alpha: 0

            }, 0.1, 0);

             tl.fromTo(closeMe, 0.5, {
                alpha: 1,
                x: 0
            }, {
                alpha: 0,
                x: -10,
                ease: Power3.easeInOut
            }, 0);

              tl.staggerFromTo(openLine, 0.5, {
                alpha: 1,
                x: 5,
                scaleX: 0
            }, {
                alpha: 1,
                x: 0,
                scaleX: 1,
                ease: Power3.easeInOut
            }, 0.1, 0.1);

            tl.call(function(){

                background.css({
                    opacity:'',
                    transform:''
                });

                 items.css({
                    opacity: '',
                    transform: ''
                });

                 openMe.css({
                    opacity: ''
                });
                openLine.css({
                    opacity: '',
                    transform: ''
                });
                closeMe.css({
                    opacity: '',
                    transform: ''
                });
                nav.removeClass('is-opened');
                nav.data('wait', false);
            });
            tl.play();

        }else {

            background.css({
                opacity:0
            });

            container.css({
                display:"block"
            });

            items.css({
                opacity: 0
            });


            var tl = new TimelineMax();
            tl.pause();
            tl.fromTo(background, 1, {
                 alpha: 1,
                 x: -$(window).width() - triangle.width(),
                  skewX: -45,
                  scaleY: 2
            },{
                alpha: 1,
                x: 0,
                skewX: 0,
                scaleY: 1,
                ease: Power3.easeInOut
            }, 0);

             tl.staggerFromTo(openLine, 0.5, {
                x: 0,
                scaleX: 1
            }, {
                x: 5,
                scaleX: 0,
                ease: Power3.easeInOut
            }, 0.1, 0);


            tl.fromTo(closeMe, 0.5, {
                alpha: 0,
                x: 10
            }, {
                alpha: 1,
                x: 0,
                ease: Power3.easeInOut
            }, 0.5);

            tl.staggerFromTo(items, 1.2, {
                alpha: 0,
                x: -80
            }, {

                 alpha: 1,
                 x: 0,
                ease: Power3.easeInOut

            }, 0.1, 0);

             tl.call(function() {

                 background.css({
                    opacity: '',
                    transform: ''
                });

                  items.css({
                    opacity: '',
                    transform: ''
                });

                  container.css({
                    display: ''
                });

                  openLine.css({
                    transform: ''
                });
                closeMe.css({
                    opacity: '',
                    transform: ''
                });

                nav.addClass('is-opened');
                 nav.data('wait', false);

             });
             tl.play();
        }

    }

    function dropDownMenu() {
            $('#menu-main li.has-sub-menu > a').append('<span class="triangle-ico"></span>');

            var dropIcon = $('#menu-main li.has-sub-menu > a').find('span');
            var subMenu = $('#menu-main li.has-sub-menu').find('.sub-menu');
            var allCollapsed = true;

            dropIcon.on("click", function(e){

                if(allCollapsed) {
                    subMenu.slideUp('medium');
                }

                e.preventDefault();
                var that = $(this);
                var openList = that.closest('li.has-sub-menu').children('.sub-menu');
                //openList.toggleClass('open-list');
                var isVisible = openList.is(":visible");

                if(!isVisible) {
                    openList.stop().slideDown('medium');
                }else {
                    openList.stop().slideUp('medium');
                }

            });

        }

    function bindClicks() {
        $('.main-nav-wrapper .nav-toggle').on('click', switchNavigation);
    }



    // Calculator functions

        openCalculatorSection = function(e) {
            e.preventDefault();
            var that = $(this);
            var targetId = that.attr('href');
            if(!targetId) return;

            that.toggleClass('calc-isOpen');
            if($('.open-calc-btn').hasClass('calc-isOpen')) {
                $('.open-calc-btn').text("Close panel");

            }else {
                $('.open-calc-btn').text("Click more");
            }

            var target = $(targetId);
            target.slideToggle('slow');
            var offset = Math.floor($('#calculator-section').offset().top - $('#main-header').outerHeight(true));
            var offsetAbout = Math.floor($('#calculator-section').offset().top - $('#about-header').outerHeight(true));

            if($('body').hasClass('page-template-page-about-php')) {
                $('html,body').stop(true).animate({

                scrollTop:offsetAbout

                },800);
                return false;
            }else {

                $('html,body').stop(true).animate({

                scrollTop:offset

                },800);
                return false;

            }

            //page-template-page-about-php

        }

        function Recalculate() {
    
            var purchasecost = parseFloat($("#purchase-cost option:selected").val());
            var ppicost = CostOfImprovements();
            var currenttaxrate = parseFloat($("#tax-rate option:selected").val());
    
            var estclaims = $("#property option:selected").val().split("-");
            var estimatedpurchaseclaim = estclaims[0];
            var estimatedppiclaim = estclaims[1];
    
            // Individual Savings
            var purchaseclaim = purchasecost * estimatedpurchaseclaim;
            var ppiclaim = ppicost * estimatedppiclaim;
    
            // CA Identified
            var caidentified = parseInt(purchaseclaim + ppiclaim);
    
            // Tax Saving
            var taxsaving = parseInt((purchaseclaim + ppiclaim) * currenttaxrate);
    
            // Update values on the screen
            $("#estimated").val(formatCurrency(caidentified));
            $("#saving").val(formatCurrency(taxsaving));
    
            // Uncomment these lines when making changes to get some debugging info
            // var msg = "Recalculating\n"
            // msg += "CA Identified: " + caidentified + "\n"
            // msg += "Purchase Claim: " + purchaseclaim + "\n"
            // msg += "PPI Claim: " + ppiclaim + "\n"
            // msg += "Tax Saving: " + taxsaving + "\n"
            // alert(msg);  
    
        }

        function CostOfImprovements() {
            
            var value = new String($("#improvement-cost").val());
            value = value.replace(/[^0-9]/g, '');
            
            return parseFloat(value);
            
        }

        function formatCurrency(num) {
            
            num = num.toString().replace(/[^0-9]/g, '');
            
            if(isNaN(num))
                num = "0";

            num = Math.floor(num * 100 + 0.50000000001);
            num = Math.floor(num / 100).toString();

            for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
                num = num.substring(0, num.length - ( 4 * i + 3))+ ',' + num.substring(num.length - ( 4 * i + 3));

            return '\u00A3' + num;
        }



        // End of calculator functions

        function showCalculator() {

            'use strict';

            var calculator = $('.icon-cal');

            if(calculator.length) {

                var position = calculator.position();
                var yPos = position.top;
                var shadow = $('.calc-shadow');
                var t = new TimelineMax({repeat: -1, yoyo: true});
                var tl = new TimelineMax({repeat: -1, yoyo: true});

                t.to(calculator, 3.0,

                        {
                            css: { top: yPos - 20},
                            ease:Power0.easeNone,
                            onComplete:function() {
                                tl.to(shadow, 3.0,

                                    {
                                        scaleX:0.7, 
                                        opacity:0.2,
                                        transformOrigin:"50% 50%", 
                                        ease:Power0.easeNone
                                    }

                                )
                            }
                        }

                    )


                t.to(calculator, 3.0, 

                    {
                        css: { top: yPos},
                        ease:Power0.easeNone,
                        onComplete:function() {
                            tl.to(shadow, 3.0,

                                    {
                                        scaleX:1.0, 
                                        opacity:0.8,
                                        transformOrigin:"50% 50%", 
                                        ease:Power0.easeNone
                                    }
                                )
                            }
                        }

                    )

                }
            
            }


    $(function(){
        bindClicks();
        mainNavigationWrapper.setNavHeight();
        containerWidth();
        setHeightMedia();
        showCalculator();
        dropDownMenu();





        //customPagination();
        $('.open-calc-btn').on('click', openCalculatorSection);

        // if the Ownership dropdown is changed, update the tax rates to match
        $("#ownership").change(function() {
                
            var taxrate = $("#tax-rate");   
            
            if ($("#ownership option:selected").text() == "Ltd Company") {
                // show Ltd Company rates
                $("#tax-rate option").remove();
                $(taxrate).append('<option value="0.20">20% - Small Companies Rate</option>');
                $(taxrate).append('<option value="0.23">23% - Full Rate</option>');
                $(taxrate).append('<option value="0.2375">23.75% - Marginal Rate</option>');                
            }
            else {
                // show Other tax rates
                $("#tax-rate option").remove();
                $(taxrate).append('<option value="0.20">20% - Basic Rate</option>');
                $(taxrate).append('<option value="0.40">40% - Higher Rate</option>');
                $(taxrate).append('<option value="0.45">45% - Additional Rate</option>');
            }
            
            Recalculate();
            
        });


        // Improvements is the only thing the User enters, so double check it
        // It's given special treatment to stop people entering characters/symbols
        // also updates on every key press, so removes the need for an "Update" button
        $("#improvement-cost").keyup(function() {
            $("#improvement-cost").val(formatCurrency($("#improvement-cost").val()));
            Recalculate();
        });     
        
        // everything else is handled here
        $("#property, #purchase-cost, #tax-rate").change(function() {
            Recalculate();
        }); 


    }); //End of document ready

    $(window).on('scroll', function() {
        /*Do staff here*/
        var topScroll = 5;
        var winScroll = $(window).scrollTop();
        var logo = $('.header-center').find('a');

        var aboutHeader = $('#about-header');
        if(winScroll > topScroll) {
            header.addClass('scroll-header');
            $('.top-wrapper').addClass('reduce-padding');
            logo.addClass('small-logo');
            aboutHeader.addClass('header-bck');
        }else {
            header.removeClass('scroll-header');
            $('.top-wrapper').removeClass('reduce-padding');
            logo.removeClass('small-logo');
            aboutHeader.removeClass('header-bck');
        }
    });


    // Browser's handler
    function onPopStateHandler (event) {
        //State zmiana hashu(jak dojde do pierwszego - historia zakonczona, to zwroci nulla)
        var state = event.state;

        if (!state) {
            //window.history.back();
            //history.replaceState(null, document.title, location.pathname);

            

                if($('body').hasClass('parent-pageid-193')) {
                    history.replaceState(null, document.title, location.pathname);
                    setTimeout(function(){

                        location.replace("../capital-allowance/case-study-capital-allowance");

                    },0);

                } else if($('body').hasClass('parent-pageid-232')) {
                    history.replaceState(null, document.title, location.pathname);

                    setTimeout(function(){

                        location.replace("../research-development/case-study-research-development");

                    },0);

                } else if ($('body').hasClass('parent-pageid-256')) {

                    setTimeout(function(){

                        location.replace("../partners/case-study-partners");

                    },0);

                }


        } else {
            window.history.back();
        }
    }



    $(window).on('load', function() {

        
            var caseWrapperOffset = Math.floor($('.case-wrapper-single').offset().top);

            setTimeout(function(){

                $('html, body').animate({

                scrollTop: caseWrapperOffset

                },800);

            },150)

    

        TweenMax.to($('html'),0.5,
                {
                    'background-color':'#ffffff',
                    ease:Power4.easeInOut
                }
            )
            
        $('body').not('.page-template-page-category-case').css({
            opacity:0,
            top: 300
        });


         TweenMax.to($('body'),1.0,
                {
                    opacity:1,
                    ease: SlowMo.easeIn
                }
            )

         TweenMax.to($('body'),1.0,
                {
                    opacity:1,
                    top:0,
                    ease:Power4.easeInOut
                    
                }
            )

         setTimeout(function () {
            window.onpopstate = onPopStateHandler;
        },1000);

         window.addEventListener('unload', function(event) {
            var caseLink = $('.item-inner').find('a');
            var caseStudyTile = $('.masonry').find('.item');


            TweenMax.to(caseStudyTile, 0.5, 
                    {
                        scale:1,
                        opacity:1,
                         ease:Power4.easeOut
                    }

                )

        });

        

         setTimeout(function(){
            //$('.section').sectionHeight();
            linksPag.on('click', scrollToSection);
            findHash();

            if(window.location.hash==="") {
                /*Metoda jest wykonywana z window.navigation dlatego jest true*/
                gotoSlide(currentSlideID, true);
            }

         },100);

    

    }).on('resize', function(){
        //$('.section').sectionHeight();
        mainNavigationWrapper.setNavHeight();
        containerWidth();
        setHeightMedia();

        // Update for current animation position
        refresh = true;
        gotoSlide(currentSlideID);
        

    });


    
})(jQuery);