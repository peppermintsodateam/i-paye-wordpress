fullscreen();

jQuery(window).resize(fullscreen);

function fullscreen() {
	var masthead = jQuery('.masthead');
    var headerHeight = jQuery('#main-header').outerHeight();
    var windowH = jQuery(window).height() - headerHeight;
    var windowW = jQuery(window).width();

    masthead.width(windowW);
    masthead.height(windowH);
}