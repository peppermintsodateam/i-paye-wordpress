(function($){

	var controls = $('#video-controls');
	var mastheadSquare = $('.mask-hover');

	function hideControls() {

		mastheadSquare.on('mouseenter', function(){
			controls.stop().animate({
				opacity:1
			},200);
		}).on('mouseleave',function(){
			controls.stop().animate({
				opacity:0
			},200);
		});

	
		controls.on('mouseenter', function(){
			controls.stop().animate({
				opacity:1
			},200);
		}).on('mouseleave',function(){
			controls.stop().animate({
				opacity:0
			},300);
		});
	}

	var video = $('#video-catax').get(0);
	var playButton = $("#play-pause");

	function bindVideo() {

		$('.mask-video').on('click', function(){
			var that = $(this);
			 video.play();
			 that.css("visibility", "hidden");
			 $("#play-pause").removeClass('play');
			  $("#play-pause").addClass('pause');
		});



		$('#video-catax').bind("pause ended", function() {
    		$('.mask-video').css("visibility", "visible");

    		// Do action when video bar is ended
    		$("#play-pause").removeClass('pause');
			  $("#play-pause").addClass('play');

    	});
			if($(window).width() < 992) {
				video.pause();
			}
		}


	function playPauseVideo() {
		var video = $('#video-catax').get(0);
		var playButton = $("#play-pause");

		playButton.on('click', function(){
			$('.mask-video').css("visibility", "hidden");
		});
	}

	$(function(){
		bindVideo();
		$("video").prop('false', true);
		hideControls();
	});
	
	$(window).on('load', function(){

		// Video
		playPauseVideo();
		var video = document.getElementById("video-catax");

		// Buttons
		var playButton = document.getElementById("play-pause");
		// Sliders
		var seekBar = document.getElementById("seek-bar");
		var fullScreenButton = document.getElementById("full-screen");



	// Event listener for the full-screen button
		fullScreenButton.addEventListener("click", function() {

		  if (video.requestFullscreen) {
		    video.requestFullscreen();
		  } else if (video.mozRequestFullScreen) {
		    video.mozRequestFullScreen(); // Firefox
		  } else if (video.webkitRequestFullscreen) {
		    video.webkitRequestFullscreen(); // Chrome and Safari
		  } else if (video.msRequestFullscreen) {
		  	video.msRequestFullscreen();
		  	video.style['-ms-transform'] = 'translate(0, 0)';
		  }

		});


		$(document).keyup(function(e) {
     		if (e.keyCode == 27) { // escape key maps to keycode `27`
        		video.style['-ms-transform'] = 'translate(-50%, -50%)';
    		}
		});



	// Event listener for the play/pause button
	playButton.addEventListener("click", function() {
		if (video.paused == true) {
			// Play the video
			video.play();
			
			playButton.classList.remove('play');
			playButton.classList.add('pause');
			$('.mask-video').css("visibility", "hidden");

		} else {
			// Pause the video
			video.pause();

			// Update the button text to 'Play'
			playButton.classList.remove('pause');
			playButton.classList.add('play');
			$('.mask-video').css("visibility", "visible");
		
			

		}
	});

	$(window).on('resize', function(){

		 if($(window).width() < 992) {
				$("#play-pause").toggleClass('pause');
				video.pause();
			}

	});

	// Event listener for the seek bar
	seekBar.addEventListener("change", function() {
		// Calculate the new time
		var time = video.duration * (seekBar.value / 100);
		video.currentTime = time;
	});

	
	// Update the seek bar as the video plays
	video.addEventListener("timeupdate", function() {
		// Calculate the slider value
		var value = (100 / video.duration) * video.currentTime;

		// Update the slider value
		seekBar.value = value;
	});

	// Pause the video when the seek handle is being dragged
	seekBar.addEventListener("mousedown", function() {
		video.pause();

		playButton.classList.remove('play');
		playButton.classList.add('pause');
		$('.mask-video').css("visibility", "hidden");
	});

	// Play the video when the seek handle is dropped
	seekBar.addEventListener("mouseup", function() {
		video.play();
		$('.mask-video').css("visibility", "hidden");

		playButton.classList.remove('play');
		playButton.classList.add('pause');
	});

});
	
})(jQuery);