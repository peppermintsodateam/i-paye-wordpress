document.addEventListener("DOMContentLoaded", function(event) { 
	var touchSlider = document.querySelector('#touch-slider');
	var slidesContainer = document.querySelector('.slides-container');
	var slides = slidesContainer.querySelectorAll('.section');
	var slidesCount = slides.length;
	var slideWidth = slides[0].width;
	var currentSlide = 0;

	var animate = function(moveBy) {
        $(slidesContainer).css('transform', 'translate('+moveBy+'%, 0)');
    }


	var hammer = Hammer(slidesContainer, {

		prevent_default: true

	});

	hammer.on('dragleft dragright swipeleft swiperight', function(e){
            var slideOffset = -(100/slidesCount)*currentSlideID;
            var dragOffset = ((100/slidesWidth)*e.gesture.deltaX) / slidesCount;

             // We define variable which fires dragging (movement)
             var m = slideOffset + dragOffset;

             animate(m);
        });
});


