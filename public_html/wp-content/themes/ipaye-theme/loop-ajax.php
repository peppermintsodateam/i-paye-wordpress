<?php 
  define('WP_USE_THEMES', false);
    require_once('../../../wp/wp-load.php');

    //echo dirname(__FILE__); - wyświetla katalog z danym plikiem

    if(isset($_GET['GETposts'])) {
        $GETposts = $_GET['GETposts'];
    }else {
        $GETposts = 0;
    }


    if(isset($_GET['GETpage'])) {
        $GETpage = $_GET['GETpage'];
    }else {
        $GETpage = 0;
    }


    $args = array(

        'post_type'=>'post',
        'posts_per_page'=>$GETposts,
        'order'=>'DESC',
        'orderby' => 'post_date',
        'paged'=>$GETpage
    );

    $posts_loop = new WP_Query($args);
?>


<?php if($posts_loop->have_posts() ) : ?>
     <?php while($posts_loop->have_posts() ) : $posts_loop->the_post(); 
            $termsArray = get_the_terms( $post->ID, "category" );
            $termsString = ""; 

             foreach ( $termsArray as $term ) { // for each term 
            $termsString .= $term->slug.' '; //create a string that has all the slugs 
        }

    ?>

    <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'custom-image' );?>

    <div class="blog-post mix <?php echo $termsString; ?>">
            <a class="blog-post-link" href="<?php the_permalink(); ?>">
                <div class="blog-post-content blog-link-img" style="background-image: url('<?php echo $thumb['0'];?>')">
                    <div class="mask-blog"></div>
                    <div class="blog-inner-content">
                      <div class="date-content">
                        <span><?php echo get_the_date('M jS, Y'); ?></span>
                      </div>
                      <h3 class="blog-header">
                        <span><?php the_title(); ?></span>
                      </h3>
                    </div>
                </div>
            </a>
        </div>

        <?php endwhile; ?>
<?php endif; wp_reset_query(); ?>



