<?php
/*
  Template Name: Page team
*/
 ?>

<?php get_header(); ?>



    <body <?php body_class(); ?>>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->


    <?php get_template_part('templates/form','panel');  ?>



        <?php

            if(is_page(186)) {
                get_template_part('templates/navigation','main');
            } else if(is_page(798)) {
                 get_template_part('templates/navigation','green');
            } else if(is_page(800)) {
                get_template_part('templates/navigation','orange');
            }

          ?>

       

        <header class="home-header page-header">
           
            <div class="navigation-switcher">
                <div class="switcher-wrapper">
                   <div id="switcher">
                       <span class="slice slice1"></span>
                       <span class="slice slice2"></span>
                       <span class="slice slice3"></span>
                   </div>
                </div>
            </div>
            <?php

                if(is_page(186)) {
                     get_template_part('templates/back','ipaye');
                } else if(is_page(798)) {
                      get_template_part('templates/back','accountancy');
                } else if(is_page(800)) {
                     get_template_part('templates/back','spi');
                }

          ?>
        </header>

        <section class="main-wrapper">

         <?php

            if(is_page(186)) {
                get_sidebar('team');
            } else if(is_page(798)) {
                get_sidebar('green');
            } else if(is_page(800)) {
                get_sidebar('orange');
            }

          ?>

          <div class="tiles-wrapper">

            <section class="company-bio">

             <?php

                if(is_page(186)) {
                     get_template_part('loops/loop','team');
                } else if(is_page(798)) {
                    get_template_part('loops/loop','teamacc');
                } else if(is_page(800)) {
                    get_template_part('loops/loop','teamspi');
                }

            ?>

            </section>

              <div class="shape-list inside-container">

              <?php

                if(is_page(186)) {
                     get_template_part('loops/loop','teamb');
                } else if(is_page(798)) {
                    get_template_part('loops/loop','teamaccb');
                } else if(is_page(800)) {
                    get_template_part('loops/loop','teamspib');
                }

            ?>
                  

                </div><!-- End of shape list div -->
              </div>


              <?php

                if(is_page(186)) {
                    get_template_part('templates/footer','hexagon');
                } else if(is_page(798)) {
                    get_template_part('templates/footer','hexagonacc');
                } else if(is_page(800)) {
                    get_template_part('templates/footer','hexagonspi');
                }

            ?>

    
        </section>


<?php get_footer(); ?>


    </body>
</html>
