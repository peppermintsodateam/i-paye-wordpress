<?php if( have_rows('download_list') ): ?>

    <div class="downloads-inner">

        <?php while( have_rows('download_list') ): the_row();

            $icon_image = get_sub_field('icon_image');
              if($icon_image) {
                echo wp_get_attachment_image($icon_image);
              }
            ?>

        <div class="download-item">
            <div style="background-image: url('<?php echo $icon_image; ?>')" class="download-ico"></div>

             <a class="download-link" download="<?php echo basename (get_sub_field('download_link') ); ?>" href="<?php the_sub_field('download_link'); ?>"><?php the_sub_field('download_text'); ?></a>
        </div>

         <?php endwhile; ?>

    </div>

<?php endif; ?>