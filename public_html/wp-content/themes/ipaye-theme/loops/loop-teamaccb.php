<?php 
                $paged = (get_query_var('paged')) ? get_query_var('paged') :1;
                $args = array(

                    'post_type'=>'team_acc',
                    'posts_per_page'=> -1,
                    'post_status' => 'publish',
                    'paged' => $paged

                );

                $loop_team = new WP_Query($args);
              ?>

              <?php if($loop_team->have_posts() ) : ?>

                <?php while($loop_team->have_posts() ) :  $loop_team->the_post(); ?>
                    
                    <div data-case-link="<?php the_field('data_name'); ?>" class="hex-item team-member team-member-purple">
                        <div class="inner">
                            <div class="inner-inner" style="background-image: url('<?php the_field('member_photo'); ?>')">

                                <div class="frontface">
                                    <header class="front-header-center">
                                      <h3 class="team-name"><?php the_field('member_name'); ?></h3>
                                      <h4 class="team-title"><?php the_field('member_title'); ?></h4>
                                    </header>
                                </div> 

                                <div class="backface purple-face">
                                      <div class="inner-text">

                                      <header class="back-header-center">
                                        <h3 class="hex-title"><?php the_field('member_name'); ?></h3>
                                        <h4 class="hex-sub-title"><?php the_field('member_title'); ?></h4>
                                      </header>
                                        
                                       <p><?php the_excerpt_max_charlength(120); ?></p>

                                        <span class="hex-btn back-btn">
                                          view more
                                        </span>

                                      </div>
                                </div>


                            </div>
                        </div>
                    </div>
                

                <?php endwhile; wp_reset_query(); ?>


              <?php endif; ?>