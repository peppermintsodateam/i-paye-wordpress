 <?php 
                $paged = (get_query_var('paged')) ? get_query_var('paged') :1;
                $args = array(

                    'post_type'=>'team_acc',
                    'posts_per_page'=> -1,
                    'post_status' => 'publish',
                    'paged' => $paged

                );

                $loop_team = new WP_Query($args);
              ?>


              <?php if($loop_team->have_posts() ) : ?>


                <?php while($loop_team->have_posts() ) :  $loop_team->the_post(); ?>

                        <article data-case-link="<?php the_field('data_name'); ?>" class="bio-description">
                            <div class="inner-wrapper-bio">
                                <div class="close-panel-team"></div>
                                <div class="bio-top-header">

                                    <div class="team-thumb">
                                        <?php 
                                            if(has_post_thumbnail()) {
                                                the_post_thumbnail('large', array('alt'=>get_the_title())); 
                                            }
                                        ?>
                                    </div>

                                    <header class="bio-inner-title">
                                        <h2 class="person-title"><?php the_field('member_name'); ?></h2>
                                        <h3 class="position-bio"><?php the_field('member_title'); ?></h3>
                                    </header>

                                </div>

                                <?php the_content(); ?>
                            </div>
                        </article>

                <?php endwhile; wp_reset_query(); ?>


              <?php endif; ?>