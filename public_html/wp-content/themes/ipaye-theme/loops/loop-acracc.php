<?php 
                             $args = array(

                                'posts_per_page'=>-1,
                                'post_type'=> 'accred_acc_logo'
                          );

                             $logo_loop = new WP_Query($args);
                         ?>

                         <?php if($logo_loop->have_posts() ) : ?>

                            <?php while($logo_loop->have_posts() ) : $logo_loop->the_post(); ?>

                              <div class="grid-item logo-item md-4 sm-6 sx-12">
                                  <figure class="logo-container">

                                  <?php if(has_post_thumbnail() ) : ?>

                                    <?php the_post_thumbnail('full', array('alt'=>get_the_title(), 'class'=>'img-responsive')); ?>

                                  <?php endif; ?>
                                     
                                  </figure>
                              </div>

                          <?php endwhile; ?>

                          <?php endif; wp_reset_query(); ?>