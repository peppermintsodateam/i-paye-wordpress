          <?php 
                $args = array(

                    'posts_per_page'=> -1,
                    'post_type' => 'testimonials_for_spi'


                );

                $testimonials_loop = new WP_query($args);
             ?>

             <?php if($testimonials_loop->have_posts() ) : ?>

                

                <div class="testimonials-slider owl-carousel">

                  <?php while($testimonials_loop->have_posts() ) : $testimonials_loop->the_post(); ?>

                        <div class="slide-testimonials">
                            <div class="testimonial-wrapper orange-wrapper">
                               <div class="testimonial-inner">
                                  <header class="testimonial-headers">
                                    <h3 class="testimonial-name"><?php the_field('name_header'); ?></h3>
                                    <h4 class="testimonial-company"><?php the_field('position_header'); ?></h4>
                                  </header>
                                  <?php the_content(); ?>
                               </div>

                              <div style="background-image: url('<?php the_field ('middle_circle_img'); ?>')"  class="image-circle middle-circle"></div>
                              <div style="background-image: url('<?php the_field ('small_circle_img'); ?>')" class="image-circle small-circle"></div>
                              <div style="background-image: url('<?php the_field ('bottom_circle_img'); ?>')" class="image-circle bottom-circle"></div>


                            </div>
                        </div>

                  <?php endwhile; ?>

                  <?php wp_reset_postdata(); ?>  

                </div>

                <?php endif;?>
               

               <footer class="footer-testimonial-rotator">
                 <span class="arrows prev purple-arrow"></span>
                 <span class="arrows next purple-arrow"></span>
               </footer>