<?php if(have_rows('table_data') ): ?>

      <table class="contractors-table green-table" cellspacing="0" cellpadding="0">
        <thead>
          <tr>

            <th class="umbrella-header" scope="col"><?php the_field('table_header_left') ?></th>
            <th class="ltd-header" scope="col"><?php the_field('table_header_right') ?></th>
          </tr>

        </thead>

        <?php while(have_rows('table_data') ) : the_row(); ?>


          <tr>
            <td data-label="umbrella"><?php the_sub_field('left_td'); ?></td>
            <td data-label="ltd"><?php the_sub_field('right_td'); ?></td>
          </tr>

        <?php endwhile; ?>
      </table>

<?php endif; wp_reset_query(); ?>