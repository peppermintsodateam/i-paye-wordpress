<div class="right-content-slogan">
	<?php the_field('right_content'); ?>
	<a href="<?php the_field('right_content_url'); ?>" class="cta-btn white-btn slide-btn">
		<?php the_field('right_content_link_text'); ?>
	</a>
</div>