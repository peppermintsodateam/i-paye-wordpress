<?php if( have_rows('download_list') ): ?>

    <div class="downloads-inner">

       <?php

        $args = array(

          'post_type'=>'sdm_downloads',
          'sdm_categories'=>'working-overseas',
          'posts_per_page'=>-1

        );

        $loop_d = new WP_Query($args);

       ?>

       <?php if($loop_d->have_posts() ) : ?>

        <div class="download_list clearfix">
          <?php while($loop_d->have_posts() ) : $loop_d->the_post(); ?>
            <div class="download-section-area">
                <div class="download-wrapp">

              <?php $icon_image = get_field('icon');
                if($icon_image) {
                  echo wp_get_attachment_image($icon_image);
                }
              ?>

              <div style="background-image: url('<?php echo $icon_image; ?>')" class="icon-pdf"></div>
              <div class="link-wrapper">
                <a class="<?php the_field('password_class'); ?>" target="_blank" href="<?php the_field('get_password_link'); ?>"><?php the_field('get_password_text'); ?></a>
              </div>
            </div>
            <?php the_content(); ?>
            </div>
          

        <?php endwhile; ?>
        </div>

       <?php endif; wp_reset_query(); ?>

    </div>

<?php endif; ?>
