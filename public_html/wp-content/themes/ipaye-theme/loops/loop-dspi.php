 <div class="downloads-inner">
   <ul class="category-filter-list" id="controls">
    <li data-filter="*" class="filter-category d-btn orange-d-btn all-btn"><span>All downloads</span></li>
 
      <?php

      /*sdm_categories comes from libs folder for custom categories slug parent it is parent number from url bar for sub categories*/
      $terms = get_terms('sdm_categories', array('parent' => 13));
      $count = count($terms);

      if($count > 0) {
        foreach ($terms as $term) {
           echo "<li class='filter-category d-btn orange-d-btn' data-filter='.".$term->slug."'><span>" . $term->name . "</span></li>";
        }
      }

       ?>
     </ul>

     <section class="clearfix" id="category-filter-section">
      <?php

              $args = array(
                    'post_type'=>'sdm_downloads',
                    'post_status'=>'publish',
                    'posts_per_page'=>-1,
                    'sdm_categories'=>'download-spi',
              );

              $download_loop = new WP_Query($args);

             ?>

             <?php if($download_loop->have_posts() ) : ?>

         <?php while($download_loop->have_posts() ) : $download_loop->the_post(); 
                
                 $termsArray = get_the_terms( $post->ID, "sdm_categories" );
                $termsString = ""; 

                 foreach ( $termsArray as $term ) { // for each term 
                $termsString .= $term->slug.' '; //create a string that has all the slugs 
            }

        ?>

         <div class="download-post mix <?php echo $termsString; ?>">

                <div class="download-wrapp">

              <?php $icon_image = get_field('icon');
                if($icon_image) {
                  echo wp_get_attachment_image($icon_image);
                }
              ?>

              <div style="background-image: url('<?php echo $icon_image; ?>')" class="icon-pdf"></div>
              <div class="link-wrapper">
                <a class="<?php the_field('password_class'); ?>" target="_blank" href="<?php the_field('get_password_link'); ?>"><?php the_field('get_password_text'); ?></a>
              </div>
            </div>

            <?php the_content(); ?>

          </div>

      

      <?php endwhile; ?>


  <?php endif; wp_reset_query(); ?>


     </section>
 </div>

 