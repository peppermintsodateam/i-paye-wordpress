<?php if(have_rows('buttons_list') ) : ?>

<ul class="countries-choose">
	<?php while(have_rows('buttons_list') ) : the_row(); ?>

		<?php 

			$btn_class = get_sub_field('button_class'); 
			$btn_text = get_sub_field('button_text'); 
			$btn_link = get_sub_field('btn_link');

		?>

		<li>
			<a class="<?php echo  $btn_class ?>" href="<?php echo  $btn_link; ?>"><?php echo $btn_text; ?></a>
		</li>

	<?php endwhile; ?>
</ul>

<?php endif; ?>