<?php
/*
  Template Name: Page I-paye
*/
 ?>


 <?php get_header(); ?>


    <body <?php body_class(); ?>>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

      <?php get_template_part('templates/form','panel');  ?>
      <?php get_template_part('templates/navigation','main'); ?>

        <header class="home-header page-header">
           

            <div class="navigation-switcher">
                <div class="switcher-wrapper">
                   <div id="switcher">
                       <span class="slice slice1"></span>
                       <span class="slice slice2"></span>
                       <span class="slice slice3"></span>
                   </div>
                </div>
            </div>
            <a href="<?php echo esc_url(home_url('/') ); ?>" class="back-btn back-home">Home</a>

            
        </header>

        <section class="main-wrapper">

          <?php get_sidebar('purple'); ?>

          <div class="tiles-wrapper">

            <div class="flip-tiles-wrapper inside-container">

                <div class="tile top-left flip-container container-hover">
                  <a href="<?php echo get_page_link(370); ?>" class="main-tile-link">
                    <div class="flipper">
                    <div class="front shape-card front-card">
                      <div class="curve-bottom-top">
                        <figure class="icon-wrapper">
                          <img src="<?php echo THEME_PATH; ?>/gfx/money-ico.png" alt="What’s in it for me?" class="img-responsive">
                        </figure>
                        <h2 class="header-tile">
                          <span>What’s in it</span>
                          <span>for me?</span>
                        </h2>
                      </div>
                    </div>

                    <div class="back shape-card back-bottom-curve purple-back">

                        <div class="back-text-wrapper">
                           <div class="back-text-inner">
                             <h3 class="tile-back-header">What’s in it for me?</h3>
                              <p>As an employee of i-paye, you’ll get all the security and benefits of being employed.  Plus we’ll take care of liaising with your clients, raising the invoices, signing the contracts and much more, leaving you to concentrate solely on the job in hand. </p>
                           </div>
                           <span class="tile-cta tile-white-cta tile-btn-left">read more</span>
                        </div>
                      
                    </div>
                  </div>
                  </a>
                </div>

                <div class="tile tile-white flip-container">
                  <div class="flipper">

                    <div class="front face-white">
                      <div class="no-curve-wrapper white-no-curve">
                       <h2 class="header-tile no-align">
                         <span>The freedom of</span>
                         <span>independence</span>
                         <span>The security</span>
                         <span>of being</span>
                         <span>employed</span>
                       </h2>
                      </div>
                    </div>

                    <div class="back face-purple"></div>

                  </div>
                </div>

                <div class="tile tile-purple-height top-tile-right flip-container container-hover">

                  <a href="<?php echo get_page_link(1017); ?>" class="main-tile-link">
                      <div class="flipper">
                        <div class="front face-purple top-right">
                          <div class="top-curve-middle">
                           <figure class="icon-wrapper">
                             <img src="<?php echo THEME_PATH; ?>/gfx/piggy-icon.png" alt="How much will it cost" class="img-responsive">
                           </figure>
                           <h2 class="header-tile center-align">
                             <span>How much</span>
                             <span>will it cost?</span>
                           </h2>
                         </div>
                        </div>

                          <div class="back shape-card face-white">
                            <div class="back-text-wrapper">
                                 <div class="back-text-inner">
                                   <h3 class="tile-back-header">How much will it cost?</h3>
                                    <p>With several packages to choose from, we’re sure you’ll find a service that fits your precise needs.  Our entry level service starts at just £7 per week.</p>
                                 </div>
                                 <span class="tile-cta tile-white-cta tile-btn-left">read more</span>
                              </div>
                          </div>

                    </div>
                  </a>

                </div>

                <div class="tile top-tile-right flip-container container-hover">

                  <a href="<?php echo get_page_link(200); ?>" class="main-tile-link">
                    <div class="flipper">
                    <div class="front shape-card top-right">
                      <div class="top-curve-right">

                       <figure class="icon-wrapper">
                         <img src="<?php echo THEME_PATH; ?>/gfx/T-shirt_ico.png" alt="" class="img-responsive">
                       </figure>

                       <h2 class="header-tile no-align">
                         <span>Case</span>
                         <span>studies</span>
                       </h2>

                     </div>
                    </div>
                    <div class="back shape-card back-top-right purple-back">

                      <div class="back-text-wrapper">
                           <div class="back-text-inner">
                             <h3 class="tile-back-header">Case studies</h3>
                              <p>We’re not new to this. Here’s just a few ways that we’ve made some of our contract employees’ lives that bit easier, so you know what you can expect.</p>
                           </div>
                           <span class="tile-cta tile-white-cta tile-btn-left">read more</span>
                        </div>

                    </div>
                  </div>
                  </a>

                </div>

                <div class="tile curve-top flip-container container-hover">

                  <a href="<?php echo get_page_link(364); ?>" class="main-tile-link">
                    <div class="flipper">
                    <div class="front shape-card front-left">
                      <div class="curve-top-middle">
                        <figure class="icon-wrapper">
                          <img src="<?php echo THEME_PATH; ?>/gfx/boat_icon.png" alt="Working overseas?" class="img-responsive">
                        </figure>

                        <h2 class="header-tile no-align">
                          <span>Working</span>
                          <span>overseas?</span>
                        </h2>

                      </div>
                    </div>

                    <div class="back shape-card back-left purple-back">
                      <div class="back-text-wrapper">
                           <div class="back-text-inner">
                             <h3 class="tile-back-header">Working overseas?</h3>
                              <p>Working in a different country should never be considered business as usual and that is why we have a specialist team to look after your unique requirements. We’ll ensure that you are fully compliant in your host country as well as  back home in the UK.</p>
                           </div>
                           <span class="tile-cta tile-white-cta tile-btn-left">read more</span>
                        </div>
                    </div>

                  </div>
                  </a>

                </div>

                <div class="tile tile-purple-height flip-container container-hover">

                  <a href="<?php echo get_page_link(322); ?>" class="main-tile-link">
                    <div class="flipper">

                    <div class="front face-purple face-choose">
                      <div class="curve-middle-left">
                        <figure class="icon-wrapper">
                          <img src="<?php echo THEME_PATH; ?>/gfx/speaker-icon.png" alt="Why choose i-paye" class="img-responsive">
                        </figure>

                        <h2 class="header-tile center-align">
                          <span>Why choose</span>
                          <span>i-paye?</span>
                        </h2>

                      </div>
                    </div>

                    <div class="back purple-back-color">
                     <div class="back-text-wrapper">
                           <div class="back-text-inner">
                             <h3 class="tile-back-header">Why choose i-paye?</h3>
                              

                              <p>Not all umbrellas are created equal, so we understand that every contractor is different. That’s why we listen to what you need and fit our services around your needs.</p>
                           </div>
                           <span class="tile-cta tile-white-cta tile-btn-left">read more</span>
                        </div>
                    </div>

                  </div>
                  </a>

                </div>


              <div class="tile tile-purple-width flip-container">
                <div class="flipper">

                  <div class="front face-purple face-rotator">

                    <div class="curve-middle-middle testimonial-curve">
                      <h2 class="header-tile center-align no-icon-header">
                         <span>What they say</span>
                      </h2>

                      <div class="testimonial-grid-rotator">
                        <div class="viewport">

                         <?php 
                            $args = array(

                                'posts_per_page'=> 4,
                                'post_type' => 'testimonials_ipaye'


                            );

                            $testimonials_loop = new WP_query($args);
                        ?>


                          <div class="slides-wrapper">

                          <?php if($testimonials_loop->have_posts() ) : ?>

                            <?php while($testimonials_loop->have_posts() ) : $testimonials_loop->the_post(); ?>

                                <div class="slide">
                                  <div class="slide-content">
                                    <p><?php the_excerpt_max_charlength(50); ?></p>
                                    <a href="<?php echo get_page_link(173); ?>">read more</a>
                                  </div>
                                </div>

                            <?php endwhile; ?>

                            <?php wp_reset_postdata(); ?> 

                            <?php endif;?>

                          </div>
                        </div>
                      </div>

                    </div>

                  </div>

                  <div class="back face-white"></div>

                </div>
              </div>


                <div class="tile top-tile-right-curve flip-container container-hover">

                  <a href="<?php echo get_page_link(293); ?>" class="main-tile-link">
                    <div class="flipper">
                    <div class="front shape-card front-right-top">
                      <div class="middle-right-curve">
                       <figure class="icon-wrapper">
                         <img src="<?php echo THEME_PATH; ?>/gfx/books-Icon.png" alt="Resource Centre" class="img-responsive">
                       </figure>

                       <h2 class="header-tile no-align">
                         <span>Resource </span>
                        <span>centre</span>
                       </h2>

                     </div>
                    </div>

                    <div class="back shape-card front-right-bottom">
                      <div class="back-text-wrapper">
                           <div class="back-text-inner">
                             <h3 class="tile-back-header">Resource centre</h3>
                              <p>Check out our resource centre for the most up-to-date details on working overseas, current legislation and a whole lot more.</p>
                           </div>
                           <span class="tile-cta tile-white-cta tile-btn-left">read more</span>
                        </div>
                    </div>
                  </div>
                  </a>

                </div>

                <div class="tile top-left flip-container container-hover">

                   <a href="<?php echo get_page_link(276); ?>" class="main-tile-link">
                       <div class="flipper">
                          <div class="front shape-card front-card">
                            <div class="curve-bottom-left longer-tile">
                              <figure class="icon-wrapper">
                                <img src="<?php echo THEME_PATH; ?>/gfx/accreditations-logo.png" alt="Accreditations" class="img-responsive">
                              </figure>

                              <h2 class="header-tile center-position">
                                <span>Accreditations</span>
                              </h2>

                            </div>
                          </div>
                        <div class="back shape-card back-bottom-curve purple-back">
                          <div class="back-text-wrapper">
                             <div class="back-text-inner">
                               <h3 class="tile-back-header">Accreditations</h3>
                                <p>It is important to feel comfortable that your payroll provider has the processes in place to ensure the service you get is what you require.</p>
                             </div>
                           <span class="tile-cta tile-white-cta tile-btn-left">read more</span>
                        </div>
                        </div>
                    </div>
                   </a>

                </div>

                <div class="tile tile-purple-height flip-container container-hover">

                  <a href="<?php echo get_page_link(920); ?>" target="blank" class="main-tile-link">
                    <div class="flipper">

                    <div class="front face-purple face-portals">
                     <div class="curve-middle-middle online-curve">
                        <figure class="icon-wrapper">
                          <img src="<?php echo THEME_PATH; ?>/gfx/Door-icon.png" alt="Online portal" class="img-responsive">
                        </figure>

                        <h2 class="header-tile center-align">
                          <span>Online</span>
                          <span>portal</span>
                        </h2>

                      </div>
                    </div>

                    <div class="back purple-back-color">
                     <div class="back-text-wrapper">
                             <div class="back-text-inner">
                               <h3 class="tile-back-header">Online portal</h3>
                                <p>If you are a current i-paye contractor you can access our online portal here. </p>
                             </div>
                           <span class="tile-cta tile-white-cta tile-btn-left">read more</span>
                        </div>
                    </div>

                  </div>
                  </a>

                </div>

              <div class="tile tile-white flip-container">
                <div class="flipper">

                  <div class="front face-white">

                    <div class="bottom-slider-area clearfix">
                        <div class="tab-slider">

                        <div class="thumbnail-rotator owl-carousel grid-carousel">
                          
                          <?php 
                            $args = array(

                              'posts_per_page'=> -1,
                              'post_type'=>'ipaye_rotator'

                            );

                            $rotator_loop = new WP_Query($args);
                          ?>

                          

                          <?php if($rotator_loop->have_posts() ) : ?>

                            <?php while($rotator_loop->have_posts() ) : $rotator_loop->the_post(); ?>

                              <?php
                                if(has_post_thumbnail()) {
                                  $thumbnail_data = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'slide-size' );
                                  $thumbnail_url = $thumbnail_data[0];
                                }
                              ?>

                          <div class="slide-item image-item" style="background-image:url('<?php echo  $thumbnail_url ?>');"></div>

                        <?php  endwhile; wp_reset_query(); ?>

                        <?php endif; ?>
                      </div>

                    </div>
                    </div>
                 
                  </div>

                  <div class="back face-purple">
                   
                  </div>

                </div>
              </div>


              <div class="tile tile-white flip-container">
                <div class="flipper">

                  <div class="front face-white">
                    <div class="no-curve-wrapper white-no-curve">
                     <div class="tab-slider">
                      <div class="text-rotator grid-carousel owl-carousel">

                      <?php 
                            $args = array(

                              'posts_per_page'=> -1,
                              'post_type'=>'ipaye_rotator'

                            );

                            $rotator_loop = new WP_Query($args);
                          ?>


                        <?php if($rotator_loop->have_posts() ) : ?>

                            <?php while($rotator_loop->have_posts() ) : $rotator_loop->the_post(); ?>

                                 <div class="slide-item text-item">
                                    <h3 class="header-tile rotator"><?php the_content(); ?></h3>
                                 </div>

                             <?php  endwhile; wp_reset_query(); ?>

                         <?php endif; ?>
 
                      </div>
                    </div>
                    </div>
                  </div>

                  <div class="back face-purple"></div>

                </div>
              </div>
            </div>
          </div>


          <?php get_template_part('templates/footer','grid'); ?>

        </section>

       


    <?php get_footer(); ?>


    </body>
</html>
