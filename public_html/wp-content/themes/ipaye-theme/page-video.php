<?php
/*
  Template Name: Video ipaye
*/
 ?>

<?php get_header(); ?>



    <body <?php body_class(); ?>>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->


    <?php get_template_part('templates/form','panel');  ?>
    <?php get_template_part('templates/navigation','main'); ?>

       
        <header class="home-header page-header">

            <div class="navigation-switcher">
                <div class="switcher-wrapper">
                   <div id="switcher">
                       <span class="slice slice1"></span>
                       <span class="slice slice2"></span>
                       <span class="slice slice3"></span>
                   </div>
                </div>
            </div>
            <a href="<?php echo esc_url(home_url('/') ); ?>" class="back-btn back-home">Home</a>
        </header>

        <section class="main-wrapper">

             <?php get_sidebar('purple'); ?>

              <div class="tiles-wrapper">
                <div class="flip-tiles-wrapper inside-container">

                    <div class="blog-post title-blog-tile">
                   
                      <div class="blog-post-content shape-title-blog purple-blog">
                        <div class="title-inner">
                          <h2 class="blog-inner-header">
                            I-Paye Videos
                          </h2>
                        </div>
                      </div>
                    </div>

                  <?php 
                    $paged = (get_query_var('paged')) ? get_query_var('paged') :1;

                     $args = array(

                        'post_type'=>'videos_ipaye',
                        'posts_per_page'=> -1,
                        'post_status' => 'publish',
                        'paged' => $paged

                    );

                        $loop_video = new WP_Query($args);
                    ?>

                    <?php if($loop_video->have_posts() ) : ?>

                        <?php while($loop_video->have_posts() ) :  $loop_video->the_post(); ?>

                            <div class="blog-post">
                                <a href="#post-<?php the_ID(); ?>" class="blog-post-link">
                                  <div class="blog-post-content blog-link-img" style="background-image:url('<?php the_field('video_icon'); ?>');">
                                    <div class="mask-video"></div>
                                  </div>
                                </a>
                            </div>
                        <?php endwhile; wp_reset_query(); ?>

                <?php endif; ?>

                </div>
              </div>



              <?php 
                    $paged = (get_query_var('paged')) ? get_query_var('paged') :1;

                     $args = array(

                        'post_type'=>'videos_ipaye',
                        'posts_per_page'=> -1,
                        'post_status' => 'publish',
                        'paged' => $paged

                    );

                        $loop_video = new WP_Query($args);
                    ?>



        <?php if($loop_video->have_posts() ) : ?>
            <?php while($loop_video->have_posts() ) :  $loop_video->the_post(); ?>
                <section id="post-<?php the_ID(); ?>" class="mfp-hide modal-box video-pop">
                 
                    <section class="video-content">
                      <div class="wireframe">
                        <?php the_field('video_content'); ?>
                      </div>
                    </section>
                </section>

            <?php endwhile; wp_reset_query(); ?>

             <?php endif; ?>

        
            <?php get_template_part('templates/footer','video'); ?>
        </section>


<?php get_footer(); ?>


    </body>
</html>
