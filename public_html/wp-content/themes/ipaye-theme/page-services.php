<?php
/*
	Template Name: Page services
*/
 ?>

<?php get_header(); ?>


    <body <?php body_class(); ?>>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

      <?php get_template_part('templates/form','panel');  ?>
      
    	<?php

            if(is_page(396)) {
                get_template_part('templates/navigation','main');
            } else if(is_page(464)) {
                 get_template_part('templates/navigation','green');
            } else if(is_page(470)) {
                get_template_part('templates/navigation','orange');
            }

          ?>


        <header class="home-header page-header">
           

            <div class="navigation-switcher">
                <div class="switcher-wrapper">
                   <div id="switcher">
                       <span class="slice slice1"></span>
                       <span class="slice slice2"></span>
                       <span class="slice slice3"></span>
                   </div>
                </div>
            </div>

             <?php

                if(is_page(396)) {
                    get_template_part('templates/back','ipaye');
                } else if(is_page(464)) {
                    get_template_part('templates/back','accountancy');
                } else if(is_page(470)) {
                    get_template_part('templates/back','spi');
                }

          ?>


        </header>

        <section class="main-wrapper">

          <?php

            if(is_page(396)) {
                get_sidebar('purple');
            } else if(is_page(464)) {
                get_sidebar('green');
            } else if(is_page(470)) {
                get_sidebar('orange');
            }

          ?>

         <div class="page-container">
              <div class="page-inner">
                  <section class="page-inner-header">
                      <div class="col-head logo-col flip-container-shape">
                         <div class="flipper-shape">


                          <?php

                            if(is_page(396)) {
                                get_template_part('templates/content','header');
                            } else if(is_page(464)) {
                               get_template_part('templates/content','headerg');
                            } else if(is_page(470)) {
                                 get_template_part('templates/content','headero');
                            }

                        ?>

                             

                             <div class="back-shape shape-bck"></div>  
                         </div>
                      </div>

                      <div class="col-head desc-col">
                          <div class="description-inner">
                              <div class="inner-text">
                              <?php the_field('top_short_content'); ?>
                              </div>
                          </div>
                      </div>
                  </section>

            <section class="page-content clearfix">

                <section class="main-page-content">

                             <?php if(have_posts() ) : ?>

                                <?php while(have_posts() ) : the_post(); ?>

                                    <?php the_content(); ?>

                                <?php  endwhile; ?>

                            <?php endif; wp_reset_query(); ?>

                    </section>

                    <section class="page-bottom-area">
                        <h4 class="content-title-sub orange-sub">
                        <?php the_field('call_paragraph'); ?> <a target="_blank" class="panel-contact-number" href="<?php the_field('phone_link'); ?>"><?php the_field('phone_text'); ?></a> 
                        </h4>
                    </section>
                      

                      <section class="page-contact-area">
                        <h3 class="content-title-main grey-header">For any enquiries please fill in the form below. </h3>

                        <div class="page-contant-form-wrapper">

                          <?php echo do_shortcode('[contact-form-7 id="118" title="Main contact form" html_id="page-contact-form" html_class="contact-form page-form-ipaye"]') ?>
                        </div>
                      </section>


                  </section>

              </div>
          </div>


          
          <?php
                if(is_page(396)) {
                    get_template_part('templates/footer','bottom');
                } else if(is_page(464)) {
                    get_template_part('templates/footer','bottomacc');
                } else if(is_page(470)) {
                     get_template_part('templates/footer','bottomspi');
                }
            ?>
            
        </section>

 
<?php get_footer(); ?>


    </body>
</html>
