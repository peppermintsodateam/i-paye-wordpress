<?php
/*
  Template Name: Page contact
*/
 ?>

<?php get_header('contact'); ?>

    <body <?php body_class(); ?>>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <?php get_template_part('templates/navigation','main'); ?>

    
        <header class="home-header page-header">
           
            <div class="navigation-switcher">
                <div class="switcher-wrapper">
                   <div id="switcher">
                       <span class="slice slice1"></span>
                       <span class="slice slice2"></span>
                       <span class="slice slice3"></span>
                   </div>
                </div>
            </div>
            <a href="<?php echo esc_url(home_url('/') ); ?>" class="back-btn back-home">Home</a>
        </header>

        <section class="main-wrapper">

            <?php get_sidebar('contact'); ?>

          <div class="page-container">
              <div class="page-inner">
                  

                  <section class="page-content clearfix">

                      <section class="page-contact-area">
                        <?php if(have_posts() ) : ?>

                            <?php while(have_posts() ) : the_post(); ?>

                                <?php the_content(); ?>

                            <?php endwhile; ?>

                        <?php endif; ?>

                        <div class="page-contant-form-wrapper">

                          <?php echo do_shortcode('[contact-form-7 id="118" title="Main contact form" html_id="page-contact-form" html_class="contact-form page-form-ipaye"]') ?>

                        </div>
                      </section>

                      <section class="map-content">
                        <div id="map" style="width: 100%;">
                          
                        </div>
                        <a target="_blank" class="map-link" href="https://goo.gl/maps/nwQDDQG2xxP2">Find Us on map</a>
                      </section>


                  </section>

              </div>
          </div>

         
            
        </section>

     <?php get_footer(); ?>


    </body>
</html>
