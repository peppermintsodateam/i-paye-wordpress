<?php get_header(); ?>
<?php the_post(); ?>

<?php 
  $image_header = get_field('header_bck');

  if($image_header) {
    echo wp_get_attachment_image($image_header);
  }
?>



   <body <?php body_class(); ?>>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

         <?php get_template_part('templates/navigation','main'); ?>



        <header class="home-header page-header">
           

            <div class="navigation-switcher">
                <div class="switcher-wrapper">
                   <div id="switcher">
                       <span class="slice slice1"></span>
                       <span class="slice slice2"></span>
                       <span class="slice slice3"></span>
                   </div>
                </div>
            </div>
            <a href="<?php echo get_page_link(9); ?>" class="back-btn back-home">Blog</a>
        </header>



         <section class="main-wrapper">

          <!-- Single and index panel go here -->
          <?php get_sidebar('blog'); ?>

          <div class="page-container single-news">
                <section id="js-parallax" class="sect-masthead js-height js-parallax" style="background-image: url('<?php echo  $image_header; ?>');">

                    <div class="masthead-content">
                        <header class="mast-header">
                            <h1 class="post-title"><?php the_title(); ?></h1>
                            <span class="post-meta">Written on: <?php echo get_the_date('M jS, Y'); ?></span>
                        </header>
                    </div>
                    <div class="overlay"></div>
                </section>

                <div class="page-inner single-blog">
                      
                  <section class="page-content clearfix">

                        <header class="breadcrumbs-list clearfix">
                            <ul>
                              <li><a href="<?php echo esc_url(home_url('/')); ?>">Home</a></li>
                              <li><a href="<?php echo get_page_link(9); ?>">Blog/News</a></li>
                              <li><?php the_title(); ?></li>
                            </ul>
                        </header>

                        <section class="main-page-content">

                          <?php the_content(); ?>

                          <ul class="blog-social-list white-purple-socials ">
                              <li><a target="_blank" class="twitter-purple" href="https://twitter.com/IPayeSPi"></a></li>
                              <li><a target="_blank" class="linked-purple" href="https://www.linkedin.com/company/i-paye?trk=top_nav_home"></a></li>
                              <li><a target="_blank" class="google-purple" href="https://plus.google.com/u/0/+IpayeandSPi"></a></li>
                          </ul>

                        </section>

                        <section class="page-bottom-area">
                            <!-- Next prev goes here -->
                            <?php previous_post_link( '%link', FALSE); ?>
                            <?php next_post_link( '%link',FALSE); ?>
                        </section>


                  </section>

                </div>
          </div>

          <?php get_template_part('templates/footer','bottom'); ?>
            
        </section>

    



<?php get_footer(); ?>


    </body>
</html>