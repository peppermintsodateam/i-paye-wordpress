
<?php 
/*
Template Name: Home intro
*/
?>

<?php get_header(); ?>


<body <?php body_class( array( "home")); ?>>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

      <?php get_template_part('templates/navigation','main'); ?>

        <header class="home-header">
           

            <div class="navigation-switcher home-switcher">
                <div class="switcher-wrapper">
                   <div id="switcher">
                       <span class="slice slice1"></span>
                       <span class="slice slice2"></span>
                       <span class="slice slice3"></span>
                   </div>
                </div>
            </div>
        </header>

        <?php get_template_part('templates/home','pagination') ?>

        <div id="touch-slider" class="main-slides-wrapper">

            <div class="slides-container">

                <?php 

                    $args = array(

                        'post_type'=>'home_slider',
                        'posts_per_page'=> 3,
                        'post_status' => 'publish'
                    );

                    $loop_home = new WP_Query($args);
                ?>

                <?php if($loop_home->have_posts() ) : ?>

                    <?php while ($loop_home->have_posts() ) : $loop_home->the_post(); ?>

                <section data-slider="<?php the_field('data_slider'); ?>" data-id="<?php the_field('data_id') ?>" class="section">
                <?php 
                    if(has_post_thumbnail()) {
                       //$feature_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
                      $feature_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'custom-image');
                    }
                ?>
                    <div style="background-image:url(<?php echo $feature_image[0]; ?>);" class="main-slide-content i-paye-wrapper-bck">

                    <?php 

                      $post_id = get_the_ID();
                       if ($post_id === 343) {
                            get_template_part('loops/link','portal');
                        }else if($post_id === 345) {
                            get_template_part('loops/free','agent');
                        }

                    ?>
                      <div class="slide-container">
                        <div class="content-section">
                          <div class="content-section-inner">
                            <div class="slider-column slider-column-left">

                                <div class="content-left-inside">
                                    <?php the_field('main_slogan'); ?>
                                   

                                    <?php 
                                         $post_id = get_the_ID();

                                         if ($post_id === 343) {
                                            get_template_part('loops/loop','buttons');

                                        }
                                    ?>

                                     <?php the_field('main_text'); ?>

                                      <?php  
                                        $id  = get_the_ID();
                                        if($id != 343) {
                                            get_template_part('loops/enter','btn');
                                        }
                                     ?>

                                     <?php 
                                         $post_id = get_the_ID();

                                         if ($post_id === 343) {
                                            get_template_part('loops/notsure','btn');

                                        }else if($post_id === 345) {
                                          get_template_part('loops/notsure','btn');
                                        }
                                    ?>



                                    

                
                                    <!-- Enter button goes here -->
                                </div>

                            </div>

                            <div class="slider-column slider-column-right">
                            <!-- right inner goes here -->
                             
                            </div>

                          </div>
                        </div>
                      </div>

                      <?php the_content(); ?>
                    </div>
                </section>

            <?php endwhile; ?>

            <?php endif; wp_reset_query(); ?>
             
            </div>

        </div>



<?php get_footer('home'); ?>