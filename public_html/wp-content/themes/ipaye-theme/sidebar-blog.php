<aside class="left-panel-wrapper i-paye-left-panel category-panel">
            <div class="bck-top"></div>

            <div class="left-content-scroll">
                  <div class="panel-content vertical-top-content">
                        <div class="panel-content-inside vertical-inside">
                            <!-- <figure class="logo-icon-panel">
                            
                                <img class="panel-logo" src="<?php echo THEME_PATH; ?>/gfx/logo-set.svg" alt="I-paye logo">

                            </figure>-->

                            <div class="category-wrapper">
                                <h2 class="main-title-panel-left purple-header-left">Latest News</h2>

                                <?php
                                    $args = array(

                                        'post_type'=>'post',
                                        'order'=>'DESC',
                                        'orderby' => 'post_date',
                                        'showposts'=>5,

                                    );

                                    $newest_posts = new WP_query($args);
                                 ?>

                                 <?php if($newest_posts->have_posts() ) :  ?>


                                <ul class="category-list">

                                   
                                    <li data-filter="*" class="filter-btn blog-btn all-btn"><span>All blogs</span></li>
                                    <?php 
                                        $terms = get_terms('category');
                                        $count = count($terms);

                                        if($count > 0) {
                                            foreach($terms as $term) {
                                               // echo "<li><a class='filter-btn' href='#' data-sortr='.".$term->slug."'>" . $term->name . "</a></li>";
                                                echo "<li class='filter-btn blog-btn' data-filter='.".$term->slug."'><span>" . $term->name . "</span></li>";
                                            }
                                        }
                                    ?>
                                </ul>

                            <?php endif; wp_reset_query(); ?>


                            </div>
                      </div>
                </div>
            </div>

            <div class="bck-bottom"></div>
          </aside>