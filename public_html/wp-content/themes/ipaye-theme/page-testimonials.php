<?php
/*
	Template Name: Page testimonials
*/
 ?>

<?php get_header(); ?>


    <body <?php body_class(); ?>>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

    <?php get_template_part('templates/form','panel');  ?>



    <?php
        if(is_page(173)) {
            get_template_part('templates/navigation','main'); 
        } else if(is_page(265)) {
            get_template_part('templates/navigation','green');
        } else if(is_page(267)) {
            get_template_part('templates/navigation','orange');
        }
     ?>
    

        <header class="home-header page-header">
           

            <div class="navigation-switcher">
                <div class="switcher-wrapper">
                   <div id="switcher">
                       <span class="slice slice1"></span>
                       <span class="slice slice2"></span>
                       <span class="slice slice3"></span>
                   </div>
                </div>
            </div>

            <?php
                if(is_page(173)) {
                    get_template_part('templates/back','ipaye');
                } else if(is_page(265)) {
                    get_template_part('templates/back','accountancy');
                } else if(is_page(267)) {
                    get_template_part('templates/back','spi');
                }
            ?>
            
        </header>

        <section class="main-wrapper">

    <?php
        if(is_page(173)) {
            get_sidebar('purple');
        } else if(is_page(265)) {
            get_sidebar('green');
        } else if(is_page(267)) {
            get_sidebar('orange');
        }
     ?>

          <div class="page-container">
           <div class="page-inner page-inner-full-width page-inner-height">
             <section class="testimonial-container">
                <?php
                    if(is_page(173)) {

                        //First loop goes here
                        get_template_part('loops/loop','testimonials');
            
                    } else if(is_page(265)) {
                        //Second loop goes here
                        get_template_part('loops/loop','testimonialsacc');

            
                    } else if(is_page(267)) {

                        //Third loop goes here
                         get_template_part('loops/loop','testimonialsspi');

                    }
            
                 ?>
             </section>
           </div>
          </div>


    <?php
        if(is_page(173)) {
            get_template_part('templates/footer','bottom');
        } else if(is_page(265)) {
            get_template_part('templates/footer','bottomacc');
        } else if(is_page(267)) {
             get_template_part('templates/footer','bottomspi');
        }
     ?>

            
        </section>

 
<?php get_footer(); ?>


    </body>
</html>
