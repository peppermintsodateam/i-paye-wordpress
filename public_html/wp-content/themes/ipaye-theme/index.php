<?php get_header(); ?>

<body <?php body_class(); ?>>
<!--[if lt IE 8]>
<p class="browserupgrade">
    You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">
    upgrade your browser</a> to improve your experience.
</p>
<![endif]-->

<?php get_template_part('templates/form', 'panel'); ?>
<?php get_template_part('templates/navigation', 'purple'); ?>

<header class="home-header page-header">
    <div class="navigation-switcher">
        <div class="switcher-wrapper">
            <div id="switcher">
                <span class="slice slice1"></span>
                <span class="slice slice2"></span>
                <span class="slice slice3"></span>
            </div>
        </div>
    </div>
    
    <a href="<?php echo esc_url(home_url('/')); ?>" class="back-btn back-home">Home</a>
</header>

<section class="main-wrapper">
    <?php get_sidebar('blog'); ?>

    <div class="tiles-wrapper">
        <div id="ajax-loop" class="flip-tiles-wrapper inside-container">
            <div class="blog-post title-blog-tile" data-colors="greyColor">
                <div class="blog-post-content grey-blog shape-title-blog">
                    <div class="title-inner">
                        <h2 class="blog-inner-header">
                            Blog
                        </h2>
                    </div>
                </div>
            </div>

            <div class="blog-post title-blog-tile" data-colors="purpleColor">
                <div class="blog-post-content purple-blog shape-title-blog">
                    <div class="title-inner">
                        <h2 class="blog-inner-header">
                            I-paye blog
                        </h2>
                    </div>
                </div>
            </div>
            
            <div class="blog-post title-blog-tile" data-colors="greenColor">
                <div class="blog-post-content green-blog shape-title-blog">
                    <div class="title-inner">
                        <h2 class="blog-inner-header">
                            Accountancy blog
                        </h2>
                    </div>
                </div>
            </div>
            
            <div class="blog-post title-blog-tile" data-colors="orangeColor">
                <div class="blog-post-content orange-blog shape-title-blog">
                    <div class="title-inner">
                        <h2 class="blog-inner-header">
                            SPI blog
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php get_template_part('templates/footer', 'blog'); ?>
</section>

<?php get_footer(); ?>

</body>
</html>
