<?php
/*
  Template Name: Page Accountancy
*/
 ?>


 <?php get_header(); ?>


    <body <?php body_class(); ?>>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

      <?php get_template_part('templates/form','panel');  ?>
      <?php get_template_part('templates/navigation','green'); ?>

        <header class="home-header page-header">
           

            <div class="navigation-switcher">
                <div class="switcher-wrapper">
                   <div id="switcher">
                       <span class="slice slice1"></span>
                       <span class="slice slice2"></span>
                       <span class="slice slice3"></span>
                   </div>
                </div>
            </div>
            <a href="<?php echo esc_url(home_url('/') ); ?>" class="back-btn back-home">Home</a>
            
        </header>

        <section class="main-wrapper">

          <?php get_sidebar('green'); ?>

          <div class="tiles-wrapper">

            <div class="flip-tiles-wrapper inside-container">

                <div class="tile top-left flip-container container-hover">
                  <a href="<?php echo get_page_link(422); ?>" class="main-tile-link">
                    <div class="flipper">
                    <div class="front shape-card green-card">
                      <div class="curve-bottom-top">
                        <figure class="icon-wrapper">
                          <img src="<?php echo THEME_PATH; ?>/gfx/money-ico-g.png" alt="What’s in it for me?" class="img-responsive">
                        </figure>
                        <h2 class="header-tile-green">
                          <span>What’s in it</span>
                          <span>for me?</span>
                        </h2>
                      </div>
                    </div>

                    <div class="back shape-card back-bottom-curve-green green-back">

                        <div class="back-text-wrapper-green">
                           <div class="back-text-inner">
                             <h3 class="tile-back-header">What’s in it for me?</h3>
                              <p>There are many advantages to setting up as a Limited Company – especially when you have a team of experts to offer you advice every step of the way. </p>
                           </div>
                           <span class="tile-cta tile-green-cta tile-btn-left">read more</span>
                        </div>
                      
                    </div>
                  </div>
                  </a>
                </div>

                <div class="tile tile-white flip-container">
                  <div class="flipper">

                    <div class="front face-white">
                      <div class="no-curve-wrapper green-no-curve">
                       <h2 class="header-tile no-align">
                         <span>Straight talking</span>
                         <span>accountancy service</span>
                         <span>for contractors</span>
                       </h2>
                      </div>
                    </div>

                    <div class="back face-green"></div>

                  </div>
                </div>

                <div class="tile tile-purple-height top-tile-right flip-container container-hover">
                  <a href="<?php echo get_page_link(464); ?>" class="main-tile-link">
                     <div class="flipper">
                   <div class="front face-green top-right-green">
                      <div class="top-curve-middle">
                       <figure class="icon-wrapper">
                         <img src="<?php echo THEME_PATH; ?>/gfx/piggy-icon.png" alt="" class="img-responsive">
                       </figure>
                       <h2 class="header-tile-green center-align">
                         <span>How much</span>
                         <span>will it cost?</span>
                       </h2>
                     </div>
                    </div>

                    <div class="back shape-card face-white">
                       <div class="back-text-wrapper-green">
                           <div class="back-text-inner">
                             <h3 class="tile-back-header">How much will it cost?</h3>
                              <p>It depends on how you decide to tailor your own unique service package, but our comprehensive level of service costs just £125 + VAT per month.</p>
                           </div>
                           <span class="tile-cta tile-green-cta tile-btn-left">read more</span>
                        </div>
                    </div>

                  </div>
                  </a>

                </div>

                <div class="tile top-tile-right flip-container container-hover">

                  <a href="<?php echo esc_url(home_url('/') ); ?>/#i-paye-accountancy" class="main-tile-link">
                      <div class="flipper">
                      <div class="front shape-card top-right-green">
                        <div class="top-curve-right">

                         <figure class="icon-wrapper">
                           <img src="<?php echo THEME_PATH; ?>/gfx/T-shirt_ico_g.png" alt="" class="img-responsive">
                         </figure>

                         <h2 class="header-tile no-align">
                           <span>Case</span>
                           <span>studies</span>
                         </h2>

                       </div>
                      </div>
                     <div class="back shape-card back-top-right green-back">

                        <div class="back-text-wrapper-green">
                             <div class="back-text-inner">
                               <h3 class="tile-back-header">Case studies</h3>
                                <p>We’re not new to this. Here’s just a few ways that we’ve made some of our service users' lives that bit easier, so that you know what to expect.</p>
                             </div>
                           <!-- <span class="tile-cta tile-green-cta tile-btn-left">read more</span>-->
                          </div>

                      </div>
                    </div>
                  </a>

                </div>

                <div class="tile curve-top flip-container container-hover">

                  <a href="<?php echo get_page_link(1008); ?>" class="main-tile-link">
                    <div class="flipper">
                    <div class="front shape-card front-left-green">
                      <div class="curve-top-middle">
                        <figure class="icon-wrapper">
                          <img src="<?php echo THEME_PATH; ?>/gfx/boat_icon.png" alt="" class="img-responsive">
                        </figure>

                        <h2 class="header-tile-green no-align">
                          <span>Coming to </span>
                          <span>the UK</span>
                          <span>to contract?</span>
                        </h2>

                      </div>
                    </div>

                    <div class="back shape-card back-left purple-back">
                       <div class="back-text-wrapper-green">
                           <div class="back-text-inner">
                            <h3 class="tile-back-header">Coming to the UK to contract?</h3>
                              <p>Even if you’re working outside the UK, we’ll provide you with the same level of service. We’ll ensure you’re getting paid compliantly, correctly and on time.</p>
                           </div>
                            <span class="tile-cta tile-green-cta tile-btn-left">read more</span>
                        </div>
                    </div>

                  </div>
                  </a>

                </div>

                <div class="tile tile-purple-height flip-container container-hover">

                  <a href="<?php echo get_page_link(742); ?>" class="main-tile-link">
                    <div class="flipper">

                    <div class="front face-green face-choose">
                      <div class="curve-middle-left">
                        <figure class="icon-wrapper">
                          <img src="<?php echo THEME_PATH; ?>/gfx/speaker-icon-acc.png" alt="Why choose i-paye accountancy" class="img-responsive">
                        </figure>

                         <h2 class="header-tile-green center-align">
                          <span>Why choose </span>
                          <span>i-paye accountancy?</span>
                        </h2>

                      </div>
                    </div>

                    <div class="back purple-back-color">
                     <div class="back-text-wrapper-green">
                           <div class="back-text-inner">
                             <h3 class="tile-back-header">Why choose i-paye accountancy?</h3>
                              <p>We listen to you, get to know your individual circumstances and then put our years of experience into action to create the best package for you.</p>
                           </div>
                          <span class="tile-cta tile-green-cta tile-btn-left">read more</span>
                        </div>
                    </div>

                  </div>
                  </a>

                </div>


              <div class="tile tile-purple-width flip-container">
                <div class="flipper">

                   <div class="front face-green face-rotator">

                   <div class="curve-middle-middle-green testimonial-curve">
                      <h2 class="header-tile-green center-align no-icon-header">
                         <span>What they say</span>
                      </h2>

                       <div class="testimonial-grid-rotator green-rotator">
                        <div class="viewport">

                         <?php 
                            $args = array(

                                'posts_per_page'=> 4,
                                'post_type' => 'testimonials_acco'


                            );

                            $testimonials_loop = new WP_query($args);
                        ?>


                          <div class="slides-wrapper">

                          <?php if($testimonials_loop->have_posts() ) : ?>

                            <?php while($testimonials_loop->have_posts() ) : $testimonials_loop->the_post(); ?>

                                <div class="slide">
                                  <div class="slide-content">
                                    <p><?php the_excerpt_max_charlength(50); ?></p>
                                    <a href="<?php echo get_page_link(265); ?>">read more</a>
                                  </div>
                                </div>

                            <?php endwhile; ?>

                            <?php wp_reset_postdata(); ?> 

                            <?php endif;?>

                          </div>
                        </div>
                      </div>

                    </div>

                  </div>

                  <div class="back face-white"></div>

                </div>
              </div>


                <div class="tile top-tile-right-curve flip-container container-hover">

                   <a href="<?php echo get_page_link(296); ?>" class="main-tile-link">
                    <div class="flipper">
                     <div class="front shape-card front-right-top-g">
                      <div class="middle-right-curve">
                       <figure class="icon-wrapper">
                          <img src="<?php echo THEME_PATH; ?>/gfx/books-Icon.png" alt="Resource Centre" class="img-responsive">
                       </figure>

                       <h2 class="header-tile-green no-align">
                         <span>Resource</span>
                         <span>centre</span>
                       </h2>

                     </div>
                    </div>

                    <div class="back shape-card front-right-bottom">
                       <div class="back-text-wrapper-green">
                           <div class="back-text-inner">
                             <h3 class="tile-back-header">Resource centre</h3>
                              <p>Check out our resource centre for the most up-to-date details on working overseas, current legislation and a whole lot more.</p>
                           </div>
                           <span class="tile-cta tile-green-cta tile-btn-left">read more</span>
                        </div>
                    </div>
                  </div>
                  </a>

                </div>

                <div class="tile top-left flip-container container-hover">

                   <a href="<?php echo get_page_link(777); ?>" class="main-tile-link">
                       <div class="flipper">
                          <div class="front shape-card green-card">
                            <div class="curve-bottom-left longer-tile">
                              <figure class="icon-wrapper">
                                <img src="<?php echo THEME_PATH; ?>/gfx/accreditations-logo.png" alt="Accreditations" class="img-responsive">
                              </figure>

                               <h2 class="header-tile-green center-position">
                                <span>Accreditations</span>
                              </h2>

                            </div>
                          </div>
                        <div class="back shape-card back-bottom-curve purple-back">
                          <div class="back-text-wrapper-green">
                             <div class="back-text-inner">
                               <h3 class="tile-back-header">Accreditations</h3>
                                <p>It is important to feel comfortable that your payroll provider has the processes in place to ensure the service you get is what you require.</p>
                             </div>
                           <span class="tile-cta tile-green-cta tile-btn-left">read more</span>
                        </div>
                        </div>
                    </div>
                   </a>

                </div>

                <div class="tile tile-purple-height flip-container container-hover">

                  <a target="_blank" href="<?php echo esc_url(home_url('/') ); ?>/#i-paye-accountancy" class="main-tile-link">
                    <div class="flipper">

                     <div class="front face-green face-portals">
                     <div class="curve-middle-middle-green online-curve">

                        <figure class="icon-wrapper">
                          <img src="<?php echo THEME_PATH; ?>/gfx/free-agent-big.png" alt="Free Agent Logo" class="img-responsive">
                        </figure>

                       <h2 class="header-tile-green center-align">
                          <span>Free</span>
                          <span>Agent</span>
                        </h2>

                      </div>
                    </div>

                    <div class="back purple-back-color">
                     <div class="back-text-wrapper-green">
                             <div class="back-text-inner">
                               <h3 class="tile-back-header">FreeAgent</h3>
                                <p>Ready to take care of business?</p>
                             </div>
                           <span class="tile-cta tile-green-cta tile-btn-left">read more</span>
                        </div>
                    </div>

                  </div>
                  </a>

                </div>

              <div class="tile tile-white flip-container">
                <div class="flipper">

                  <div class="front face-white">

                    <div class="bottom-slider-area clearfix">
                        <div class="tab-slider">

                        <div class="thumbnail-rotator owl-carousel grid-carousel">
                          
                          <?php 
                            $args = array(

                              'posts_per_page'=> -1,
                              'post_type'=>'accountancy_rotator'

                            );

                            $rotator_loop = new WP_Query($args);
                          ?>

                          

                          <?php if($rotator_loop->have_posts() ) : ?>

                            <?php while($rotator_loop->have_posts() ) : $rotator_loop->the_post(); ?>

                              <?php
                                if(has_post_thumbnail()) {
                                  $thumbnail_data = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'slide-size' );
                                  $thumbnail_url = $thumbnail_data[0];
                                }
                              ?>

                          <div class="slide-item image-item" style="background-image:url('<?php echo  $thumbnail_url ?>');"></div>

                        <?php  endwhile; wp_reset_query(); ?>

                        <?php endif; ?>
                      </div>

                    </div>
                    </div>
                 
                  </div>

                  <div class="back face-purple">
                   
                  </div>

                </div>
              </div>


              <div class="tile tile-white flip-container">
                <div class="flipper">

                  <div class="front face-white">
                    <div class="no-curve-wrapper green-no-curve">
                     <div class="tab-slider">
                      <div class="text-rotator grid-carousel owl-carousel">

                      <?php 
                            $args = array(

                              'posts_per_page'=> -1,
                              'post_type'=>'accountancy_rotator'

                            );

                            $rotator_loop = new WP_Query($args);
                          ?>


                        <?php if($rotator_loop->have_posts() ) : ?>

                            <?php while($rotator_loop->have_posts() ) : $rotator_loop->the_post(); ?>

                                 <div class="slide-item text-item">
                                    <h3 class="header-tile-green rotator"><?php the_content(); ?></h3>
                                 </div>

                             <?php  endwhile; wp_reset_query(); ?>

                         <?php endif; ?>
 
                      </div>
                    </div>
                    </div>
                  </div>

                  <div class="back face-purple"></div>

                </div>
              </div>
            </div>
          </div>


          <?php get_template_part('templates/footer','gridacc'); ?>

        </section>

       


    <?php get_footer(); ?>


    </body>
</html>
