<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->

<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes('html'); ?>> <!--<![endif]-->
    <head>
        <meta charset="<?php bloginfo('charset') ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, user-scalable=no" />

        <?php 
          if((is_search())) : ?>
            <meta name="robots" content="noindex, nofollow" />
        <?php endif; ?>


        <title>
          <?php wp_title(''); ?><?php if (!(is_404()) && (is_single()) || (is_page()) || (is_category()) || (is_tag()) || (is_author()) || (is_archive())) { ?> <?php } ?>
        </title>


        <link href="https://fonts.googleapis.com/css?family=Muli:400,600,700,800,900&amp;subset=latin-ext" rel="stylesheet">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" >
        <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/gfx/favicon.ico" />

        <script>
            var _gaq = [['_setAccount', 'UA-40604881-1'], ['_trackPageview']];
            (function (d, t) {
                var g = d.createElement(t), s = d.getElementsByTagName(t)[0];
                g.src = ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js';
                s.parentNode.insertBefore(g, s)
            } (document, 'script'));
        </script>

        <?php wp_head(); ?>

        <script>
          var baseUrl = "<?php echo get_template_directory_uri(); ?>";
          var postsCount = "<?php echo get_option('posts_per_page'); ?>"
      </script>
    </head>