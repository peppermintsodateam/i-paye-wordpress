 <footer class="footer-grid footer-scroll-grid page-footer">
              <div class="footer-inner-grid">

              <?php 
                $args = array(

                'post_type'=>'footer_logo',
                'posts_per_page'=> 6
                );

                $logo_loop = new WP_Query($args);
              ?>

              <?php if($logo_loop->have_posts() ) : ?>
                <ul class="company-logos-set">
                  <?php while($logo_loop->have_posts() ) : $logo_loop->the_post(); ?>
                    <li>
                       <?php if ( has_post_thumbnail() ) : ?>
                            <?php the_post_thumbnail('full', array('alt'=>get_the_title())); ?>
                        <?php endif; ?>
                    </li>
                  <?php endwhile; ?>
                </ul>

            <?php endif; wp_reset_query(); ?>

                <div class="bottom-footer-wrapper">
                  <p class="copyright-note">&copy; Copyright I-paye <?php echo date('Y'); ?></p>

                  <?php wp_nav_menu(array(
                        'theme_location'=>'footer-menu',
                        'menu_class'=>'footer-menu-list',
                        'container' =>false,
                        'menu_id' => 'footer-menu-list'
                    ));

                    ?>


                  <p class="company-link"><span>Designed by:</span> <a target="_blank" href="http://www.peppermintsoda.co.uk">Peppermint Soda</a></p>
                </div>

              </div>
        </footer>



