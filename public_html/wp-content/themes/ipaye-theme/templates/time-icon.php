 <svg version="1.1" id="clock-product" class="product-svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 viewBox="0 0 154.9 154.5" style="enable-background:new 0 0 154.9 154.5;" xml:space="preserve">
            <g>
                <path style="fill:#FFFFFF;stroke:#ED2F3E;stroke-miterlimit:10;" d="M77.4,1.8C35.8,1.8,2,35.6,2,77.2c0,41.6,33.8,75.4,75.4,75.4
                    c41.6,0,75.4-33.8,75.4-75.4C152.8,35.6,119,1.8,77.4,1.8z"/>
                <path style="fill:#FFFFFF;stroke:#ED2F3E;stroke-miterlimit:10;" d="M77.4,13.8c-35,0-63.4,28.4-63.4,63.4
                    c0,35,28.4,63.4,63.4,63.4c35,0,63.4-28.4,63.4-63.4C140.8,42.2,112.4,13.8,77.4,13.8z"/>
                <path style="fill:#FFFFFF;" d="M77.4,18.8c32.3,0,58.4,26.1,58.4,58.4c0,32.3-26.1,58.4-58.4,58.4c-32.3,0-58.4-26.1-58.4-58.4
                    C19,45,45.1,18.8,77.4,18.8z M81.9,81.8c1.2-1.2,1.9-2.8,1.9-4.5c0-3.5-2.9-6.4-6.4-6.4c-3.5,0-6.4,2.9-6.4,6.4
                    c0,1.9,0.9,3.7,2.2,4.8c1.1,1,2.6,1.6,4.2,1.6C79.2,83.6,80.8,82.9,81.9,81.8L81.9,81.8z"/>
                <rect x="75.3" y="18.8" style="fill:#ED2F3E;" width="4.2" height="14.3"/>
                <rect x="75.3" y="121.3" style="fill:#ED2F3E;" width="4.2" height="14.3"/>
                <rect x="121.5" y="75.1" style="fill:#ED2F3E;" width="14.3" height="4.2"/>
                <rect x="19" y="75.1" style="fill:#ED2F3E;" width="14.3" height="4.2"/>
                <path style="fill:#ED2F3E;" d="M110.6,107.4l-27-27c0.5-1,0.8-2,0.8-3.2c0-3.1-2-5.7-4.8-6.6V44.8h-4.2v25.8
                    c-2.8,0.9-4.8,3.5-4.8,6.6c0,1.3,0.4,2.5,1,3.6l-26,26l3,3l26.2-26.2l-0.1-0.1c0.9,0.4,1.9,0.7,2.9,0.7c1.1,0,2.2-0.3,3.2-0.8
                    l27,27L110.6,107.4z"/>
            </g>
    </svg>