<aside class="left-panel-wrapper i-paye-left-panel">
            <div class="bck-top"></div>

            <div class="left-content-scroll">
              <div class="panel-content">
                <div class="panel-content-inside">
                    <figure class="logo-icon-panel">
                        <img class="panel-logo" src="<?php echo THEME_PATH; ?>/gfx/I-paye-logo.svg" alt="">
                    </figure>

                    <div class="content-panel-text">
                        <h1 class="main-title-panel-left purple-header-left">I-Paye Blog</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem Ipsum.</p>
                    </div>

                    <div class="panel-contact-details purple-details">
                        <p>For more information on how we can assist you with your contracting career drop us a line on: <a class="panel-contact-number" href="tel:+44161789987">0161 789987</a> 
or <a href="#" class="panel-left-email">click here</a> to send us an enquiry</p>
                    </div>

                    <ul class="social-icons-links panel-social-links purple-social">

                        <li>
                          <a href="https://twitter.com/IPAYEtweets" class="twitter-purple-b social-icon"></a>
                        </li>

                        <li>
                          <a href="https://www.linkedin.com/company-beta/1167793/" class="linked-purple-b social-icon"></a>
                        </li>

                      </ul>

                </div>
              </div>
            </div>

            <div class="bck-bottom"></div>
          </aside>