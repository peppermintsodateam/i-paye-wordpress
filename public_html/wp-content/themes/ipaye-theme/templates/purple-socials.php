<ul class="social-icons-links panel-social-nav white-purple-socials">

    <li>
      <a target="_blank" href="https://twitter.com/IPAYEtweets" class="twitter-purple social-icon"></a>
    </li>

    <li>
      <a target="_blank" href="https://www.linkedin.com/company-beta/1167793/" class="linked-purple social-icon"></a>
    </li>
</ul>