<?php if(have_rows('tabs_list_eu') ) : ?>


<ul class="list-working-eu" id="list-working-eu">

<?php while(have_rows('tabs_list_eu') ) : the_row(); ?>

  <li class="filter-category d-btn <?php the_sub_field('button_class'); ?>"><?php the_sub_field('tabs_item_eu'); ?></li>

  <?php endwhile; ?>


    
     </ul>

     <?php endif; wp_reset_query(); ?>


<?php if(have_rows('eu_content') ) : ?>
     <section class="tabs-eu clearfix">
        <?php while(have_rows('eu_content') ) : the_row(); ?>

            <div class="tabs-content-eu">
                <?php the_sub_field('eu_sections'); ?>
            </div>

        <?php endwhile;?>
     </section>

     <?php endif; wp_reset_query(); ?>