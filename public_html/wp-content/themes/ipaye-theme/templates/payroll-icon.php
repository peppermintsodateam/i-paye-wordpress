<svg version="1.1" id="payroll-product" class="product-svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="0 0 152.3 155.7" style="enable-background:new 0 0 152.3 155.7;" xml:space="preserve">
            <g>
                <path style="fill:#FFA685;" d="M126.2,89.8c1.8,3.2,2.8,6.9,2.8,10.7c0,8.4-4.7,15.8-11.7,19.5l-10.5-19.5L126.2,89.8z"/>
                <path style="fill:none;" d="M126.2,89.8l-19.4,10.7l4.3-21.7C117.6,80.1,123.1,84.2,126.2,89.8z"/>
                <path style="fill:none;" d="M106.8,122.7c-7.4,0-13.9-3.6-17.9-9.1l17.9-13l10.5,19.5C114.2,121.8,110.6,122.7,106.8,122.7z"/>
                <path style="fill:#7DE3CB;" d="M106.8,78.4c1.5,0,2.9,0.2,4.3,0.4l-4.3,21.7l-17.9,13c-2.7-3.7-4.3-8.2-4.3-13
                    C84.6,88.3,94.5,78.4,106.8,78.4z"/>
                <path style="fill:none;stroke:#ED2F3E;stroke-miterlimit:10;" d="M91.2,0.6v18.8H46c-8.3,0-15.1,6.7-15.1,15.1v68.8
                    c0,8.3-6.7,15.1-15.1,15.1c-8.3,0-15.1-6.7-15.1-15.1V0.6H91.2z"/>
                <path style="fill:#ED2F3E;" d="M46,19.5c8.3,0,15.1,6.7,15.1,15.1v83.9H15.9c8.3,0,15.1-6.7,15.1-15.1V34.5
                    C31,26.2,37.7,19.5,46,19.5z"/>
                <path style="fill:none;stroke:#ED2F3E;stroke-miterlimit:10;" d="M46,19.5c8.3,0,15.1,6.7,15.1,15.1v83.9v36.7h90.4V34.5
                    c0-8.3-6.7-15.1-15.1-15.1H91.2H46z"/>
                <rect x="77.8" y="37" style="fill:#ED2F3E;" width="22.6" height="4.5"/>
                <rect x="78.7" y="51.5" style="fill:#ED2F3E;" width="56.1" height="4.5"/>
                <rect x="78.7" y="64" style="fill:#ED2F3E;" width="56.1" height="4.5"/>
                <rect x="78.7" y="131.3" style="fill:#ED2F3E;" width="56.1" height="4.5"/>
                <g>
                    <path style="fill:#ED2F3E;" d="M111.9,80c5.4,1.3,10,4.7,12.9,9.4l-16.6,9.2L111.9,80 M111.1,78.8l-4.3,21.7l19.4-10.7
                        C123.1,84.2,117.6,80.1,111.1,78.8L111.1,78.8z"/>
                </g>
                <path style="fill:#ED2F3E;" d="M106.8,100.6l4.3-21.7c-1.4-0.3-2.8-0.4-4.3-0.4c-12.2,0-22.2,9.9-22.2,22.2c0,4.9,1.6,9.4,4.3,13
                    L106.8,100.6z"/>
                <g>
                    <path style="fill:#ED2F3E;" d="M106.5,102l9.5,17.6c-2.8,1.4-6,2.1-9.1,2.1c-6.4,0-12.5-3-16.5-7.9L106.5,102 M106.8,100.6
                        l-17.9,13c4,5.5,10.5,9.1,17.9,9.1c3.8,0,7.4-1,10.5-2.6L106.8,100.6L106.8,100.6z"/>
                </g>
                <path style="fill:#ED2F3E;" d="M117.3,120.1c6.9-3.7,11.7-11.1,11.7-19.5c0-3.9-1-7.6-2.8-10.7l-19.4,10.7L117.3,120.1z"/>
            </g>
</svg>