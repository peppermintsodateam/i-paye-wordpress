<header class="main-features features-ipaye">
                <article class="header-item">
                  <div class="content-inside">
                    <figure class="icon-header">
                      <img src="<?php the_field('icon_image'); ?>" alt="Features logo">
                    </figure>

                    <?php the_field('title'); ?>
                  </div>

                </article>

                <article class="header-item">
                  <div class="rotator-circle-bck">
                    <div class="grid-rotator clearfix owl-carousel">


                    <?php 
                        $args = array(

                            'post_type'=>'slides_fipaye',
                            'posts_per_page'=> -1

                        );

                        $posts_loop = new WP_Query($args);
                     ?>

                     <?php if($posts_loop->have_posts() ) : ?>


                        <?php while($posts_loop->have_posts() ) : $posts_loop-> the_post(); ?>

                        <div class="grid-slide">

                         <span class="rotator-currency"><?php the_field('percent'); ?></span>

                            <h2 class="header-rotator-grid">
                              <span><?php the_field('header_top'); ?></span>
                              <span><?php the_field('header_bottom'); ?></span>
                            </h2>

                           

                            <!-- <a href="<?php the_field('link_url'); ?>" class="cta-btn white-btn tiles-rotator-btn"><?php the_field('link_text'); ?></a>-->

                        </div>

                    <?php endwhile; ?>


                    <?php endif; wp_reset_query(); ?>

                       


                      </div>
                  </div>
                </article>

                <article class="header-item grouped-item">
                  <div class="double-size-inner">
                    <div class="slogan-inner">
                      <?php if(have_posts() ) : ?>

                        <?php while(have_posts() ) : the_post(); ?>

                            <?php the_content(); ?>

                            <?php endwhile ?>

                      <?php endif; wp_reset_query(); ?>
                    </div>
                  </div>
                </article>
              </header>