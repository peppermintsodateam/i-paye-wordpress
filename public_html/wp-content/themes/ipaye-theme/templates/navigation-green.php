<aside class="left-navigation home-navigation green-navigation">
          <div class="navigation-scroller">


            <?php wp_nav_menu(array(
                'theme_location'=>'main-menu',
                'menu_class'=>'main-navigation-list',
                'container' =>false,
                'menu_id' => 'main-navigation-list'
            ));

            ?>



          </div>

             <footer class="social-panel-footer">

                <?php get_template_part('templates/green','socials'); ?>

            </footer>
        </aside>