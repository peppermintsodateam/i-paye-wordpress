<svg version="1.1" id="devices" class="product-svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
             viewBox="0 0 384.5 241.5" style="enable-background:new 0 0 384.5 241.5;" xml:space="preserve">
            <path style="fill:#FFFFFF;" d="M320.4,109.9v103.8H217.5v8.3h-50.2v-8.3H64.5V109.9V47.9h32v101.9v17.5c0,5.1,4.2,9.3,9.2,9.3h79.2
                c5.1,0,9.3-4.2,9.3-9.3v-17.5V47.9h126.2V109.9z"/>
            <path style="fill:#FFFFFF;stroke:#ED2F3E;stroke-miterlimit:10;" d="M383.4,213.7c0,0,1.1,11.9,0,15.7c-0.9,3.1-6.7,11.1-6.7,11.1
                H8.2c0,0-5.8-7.9-6.7-11.1c-1.1-3.8,0-15.7,0-15.7H41h23.5h102.9v8.3h50.2v-8.3h102.9h23.5H383.4z"/>
            <path style="fill:#ED2F3E;" d="M194.2,149.8v17.5c0,5.1-4.2,9.3-9.3,9.3h-79.2c-5.1,0-9.2-4.2-9.2-9.3v-17.5H194.2z"/>
            <path style="fill:#FFFFFF;stroke:#ED2F3E;stroke-miterlimit:10;" d="M99.3,24.4v23.5H64.5v62.1v103.8H41V96.5V37.8
                c0-7.4,6-13.4,13.4-13.4H99.3z"/>
            <path style="fill:#ED2F3E;" d="M194.2,9.6v14.5H96.5V9.6c0-5.1,4.2-9.3,9.2-9.3h79.2C190,0.4,194.2,4.5,194.2,9.6z"/>
        <path style="fill:#FFFFFF;stroke:#ED2F3E;stroke-miterlimit:10;" d="M343.8,96.5v117.2h-23.5V109.9V47.9h-129V24.4h139
            c7.4,0,13.4,6,13.4,13.4V96.5z"/>
            <g>
                <rect x="216.7" y="89" style="fill:#ED2F3E;" width="76.4" height="8.4"/>
                <g>
                    <polygon style="fill:#ED2F3E;" points="229.6,104.5 218.2,93.2 229.6,81.8 220,81.8 208.6,93.2 220,104.5      "/>
                </g>
            </g>
            <g>
                <rect x="216.7" y="126.7" style="fill:#ED2F3E;" width="76.4" height="8.4"/>
                <g>
                    <polygon style="fill:#ED2F3E;" points="280.1,142.3 291.5,130.9 280.1,119.5 289.8,119.5 301.1,130.9 289.8,142.3      "/>
                </g>
            </g>
            <circle style="fill:#FFFFFF;stroke:#ED2F3E;stroke-miterlimit:10;" cx="145.3" cy="163.2" r="8"/>
            <polygon style="fill:#ED2F3E;" points="194.2,24.1 194.2,24.4 194.2,47.9 194.2,149.8 96.5,149.8 96.5,47.9 96.5,24.4 96.5,24.1 "/>
            <polygon style="fill:#FFFFFF;" points="187.3,24.1 187.3,24.4 187.3,47.9 187.3,149.8 103.3,149.8 103.3,47.9 103.3,24.4 
                103.3,24.1 "/>
    </svg>