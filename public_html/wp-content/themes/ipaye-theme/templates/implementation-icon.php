 <svg version="1.1" id="implementation" class="product-svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
         viewBox="0 0 234.3 237.3" style="enable-background:new 0 0 234.3 237.3;" xml:space="preserve">
    <g>
        <polygon style="fill:#FFFFFF;stroke:#ED2F3E;stroke-miterlimit:10;" points="232.2,124.2 232.2,180.7 175.6,180.7 175.6,152.5 
            175.6,124.2     "/>
        <polygon style="fill:#FFFFFF;stroke:#ED2F3E;stroke-miterlimit:10;" points="180.5,209.5 180.5,235 54,235 54,209.5 117.3,209.5    
            "/>
        <polygon style="fill:#FFFFFF;stroke:#ED2F3E;stroke-miterlimit:10;" points="58.9,152.5 58.9,180.7 2.4,180.7 2.4,124.2 
            58.9,124.2  "/>
        <polygon style="fill:#FFFFFF;stroke:#ED2F3E;stroke-miterlimit:10;" points="160.2,57.6 160.2,95.4 117.3,95.4 74.4,95.4 
            74.4,57.6 117.3,57.6    "/>
        <rect x="114.2" y="27" style="fill:#ED2F3E;" width="6.1" height="30.5"/>
        <rect x="114.2" y="95.4" style="fill:#ED2F3E;" width="6.1" height="40.7"/>
        <rect x="114.2" y="176.4" style="fill:#ED2F3E;" width="6.1" height="33.2"/>
        <rect x="58.9" y="149.4" style="fill:#ED2F3E;" width="34.5" height="6.1"/>
        <rect x="139.1" y="149.4" style="fill:#ED2F3E;" width="36.5" height="6.1"/>
        <polygon style="fill:#FFFFFF;stroke:#ED2F3E;stroke-miterlimit:10;" points="134.4,18.9 117.3,36 100.2,18.9 117.3,1.8     "/>
        <path style="fill:#ED2F3E;" d="M117.3,124.2c15.6,0,28.3,12.7,28.3,28.3c0,15.6-12.7,28.3-28.3,28.3c-15.6,0-28.3-12.7-28.3-28.3
            C89,136.9,101.7,124.2,117.3,124.2z"/>
    </g>
 </svg>