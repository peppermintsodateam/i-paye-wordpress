<div class="contact-form-panel purple-contant-panel">

            <header class="panel-contact-header">
                <div class="close-panel-btn"></div>
            </header>
            

            <div class="panel-contact-inner">

                <h2 class="panel-contact-title">
                    Enquiry form
                </h2>

                <section class="enquiry-text">
                    <p>Please complete the form below, so we can provide quick and efficient service.</p>
                </section>

                <div class="main-contant-form" id="panel-contact-form">

                    <?php echo do_shortcode('[contact-form-7 id="282" title="Panel contact form" html_id="main-contact-form" html_class="contact-form"]') ?>
                </div>
            </div>

            <footer class="bottom-contact-footer">
                <ul class="bottom-social-list white-purple-socials">

                  <li>
                    <a target="_blank" href="https://twitter.com/IPAYEtweets" class="twitter-purple social-icon"></a>
                  </li>

                  <li>
                    <a target="_blank" href="https://www.linkedin.com/company-beta/1167793" class="linked-purple social-icon"></a>
                  </li>
                  
                </ul>
            </footer>

        </div>