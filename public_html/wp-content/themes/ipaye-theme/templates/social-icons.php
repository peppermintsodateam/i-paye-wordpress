<?php
	$args = array(

		'post_type'=>'social_icons',
	    'posts_per_page'=> -1,
	    'post_status' => 'publish'

	);

	$social_loop = new WP_Query($args);
 ?>

<?php if($social_loop->have_posts() ) : ?>
<ul class="social-icons-links panel-social-links purple-social orange-social">
	<?php while($social_loop->have_posts() ) : $social_loop->the_post(); ?>
		<li><a target="_blank" href="<?php the_field('social_link'); ?>" class="social-icon <?php the_field('social_class'); ?>"></a></li>
	<?php endwhile; ?>
</ul>

<?php endif; wp_reset_query();?>