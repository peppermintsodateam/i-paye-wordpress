<svg version="1.1" id="computer-ico" class="product-svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="0 0 194.8 151.6" style="enable-background:new 0 0 194.8 151.6;" xml:space="preserve">
        <path style="fill:#FFFFFF;stroke:#ED2F3E;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:5;" d="M194.3,97.4v23.8
            h-76.5H77H0.6V97.4h13.1h167.5H194.3z"/>
        <path style="fill:#FFFFFF;stroke:#ED2F3E;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:5;" d="M194.3,0.5v96.8
            h-13.1V14H13.7v83.3H0.6V0.5H194.3z"/>
        <path style="fill:#FFFFFF;stroke:#ED2F3E;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:5;" d="M149,141.6v9.5H45.8
            v-9.5h26.1h51H149z"/>
        <path style="fill:#FFFFFF;stroke:#ED2F3E;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:5;" d="M117.8,121.2
            l5.1,20.4h-51l5.1-20.4H117.8z"/>
        <path style="fill:#ED2F3E;" d="M108,112.3H87.7c-1.5,0-2.6-1.2-2.6-2.6c0-1.5,1.2-2.6,2.6-2.6H108c1.5,0,2.6,1.2,2.6,2.6
            C110.7,111.1,109.5,112.3,108,112.3z"/>
        <path style="fill:#ED2F3E;" d="M167.8,112.3c-0.7,0-1.4-0.3-1.9-0.8c-0.5-0.5-0.8-1.2-0.8-1.9c0-0.7,0.3-1.4,0.8-1.9
            c1-1,2.7-1,3.7,0c0.5,0.5,0.8,1.2,0.8,1.9c0,0.7-0.3,1.4-0.8,1.9C169.2,112,168.5,112.3,167.8,112.3z"/>
        <path class="fill-path" style="fill:#ED2F3E;" d="M103.6,65.5l-1.7-1.7l12-12l1.7,1.7L103.6,65.5z"/>
        <path style="fill:#ED2F3E;" d="M136.9,57.7L120,51.9l0.8-2.3l16.9,5.8L136.9,57.7z"/>
        <path style="fill:#ED2F3E;" d="M145.1,56.5l-1.9-1.6l9.8-11.9l1.9,1.6L145.1,56.5z"/>
        <path style="fill:#ED2F3E;" d="M103.1,64.3c0.7,0.8,1.1,1.8,1.1,2.9c0,2.4-2,4.4-4.4,4.4c-2.4,0-4.4-2-4.4-4.4c0-2.4,2-4.4,4.4-4.4
            C101.1,62.8,102.2,63.4,103.1,64.3z"/>
        <path style="fill:#ED2F3E;" d="M119.9,47.8c0.7,0.8,1.1,1.8,1.1,2.9c0,2.4-2,4.4-4.4,4.4c-2.4,0-4.4-2-4.4-4.4c0-2.4,2-4.4,4.4-4.4
            C117.9,46.3,119.1,46.9,119.9,47.8z"/>
        <path style="fill:#ED2F3E;" d="M144.2,55.1c0.7,0.8,1.1,1.8,1.1,2.9c0,2.4-2,4.4-4.4,4.4c-2.4,0-4.4-2-4.4-4.4c0-2.4,2-4.4,4.4-4.4
            C142.2,53.6,143.4,54.2,144.2,55.1z"/>
        <path style="fill:#ED2F3E;" d="M159.8,37.7c0.7,0.8,1.1,1.8,1.1,2.9c0,2.4-2,4.4-4.4,4.4c-2.4,0-4.4-2-4.4-4.4c0-2.4,2-4.4,4.4-4.4
            C157.8,36.2,159,36.8,159.8,37.7z"/>
        <path style="fill:#FFFFFF;stroke:#ED2F3E;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:5;" d="M79.5,72.4v4.6H30.2
            v-4.6c0-0.3,0-0.5,0-0.8c0.4-7.8,6.5-14,14.2-14.7l-0.5-0.4h0.5c-3-2.8-4.8-6.8-4.8-11l0-0.5c0.4-8.2,7.1-14.5,15.2-14.5
            c8.1,0,14.8,6.4,15.3,14.5l0,0.6c0,4.2-1.8,8.1-4.8,11h0.5l-0.5,0.4c7.7,0.7,13.8,6.9,14.2,14.7C79.5,71.8,79.5,72.1,79.5,72.4z"/>
   </svg>