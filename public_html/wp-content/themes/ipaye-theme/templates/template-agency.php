
<?php if(have_rows('tabs_list') ) : ?>


<ul class="filter-list" id="filter-list">

<?php while(have_rows('tabs_list') ) : the_row(); ?>

  <li class="filter-category d-btn <?php the_sub_field('button_class'); ?>"><?php the_sub_field('tab_label');?></li>

  <?php endwhile; ?>


    
     </ul>

     <?php endif; wp_reset_query(); ?>


<?php if(have_rows('section') ) : ?>
     <section class="tabs clearfix">
     	<?php while(have_rows('section') ) : the_row(); ?>

     		<div class="tabs-content"><?php the_sub_field('content'); ?></div>

     	<?php endwhile;?>
     </section>

     <?php endif; wp_reset_query(); ?>