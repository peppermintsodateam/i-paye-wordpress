<div class="front-shape orange-shape shape-bck">
                                 <div class="shape-content">
                                    <figure class="title-ico">
                                      <?php if ( has_post_thumbnail() ) : ?>
                                        <?php the_post_thumbnail('full', array('alt'=>get_the_title())); ?>
                                      <?php endif; ?>

                                      
                                    </figure>
                                    <h2 class="main-shape-title">
                                        <span><?php the_field('top_header'); ?></span>
                                      <span><?php the_field('bottom_header'); ?></span>
                                    </h2>
                                 </div>
                             </div>