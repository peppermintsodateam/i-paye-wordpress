<nav class="pagination-nav">
            <div class="pagination-wrapper">
                <ul class="pagination-list clearfix">
                    <li class="ipaye-logo"><span data-slider="i-paye"><img src="<?php echo THEME_PATH; ?>/gfx/I-paye-logo.svg" alt="I-paye"></span></li>
                    <li class="account-logo"><span data-slider="i-paye-accountancy"><img src="<?php echo THEME_PATH; ?>/gfx/i-paye-accountancy-logo.svg" alt="I-paye accountancy"></span></li>
                    <li class="spi-logo"><span data-slider="spi-accountancy"><img src="<?php echo THEME_PATH; ?>/gfx/spi-accountancy-logo.svg" alt="Spi accountancy"></span></li>
                </ul>
            </div>
        </nav>