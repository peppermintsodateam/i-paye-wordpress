<?php 
                $args = array(

                    'post_type'=>'features_accountancy',
                    'posts_per_page'=> 12

                );

                    $new_loop = new WP_Query($args);
               ?>

               <?php if($new_loop->have_posts() ) : ?>

                <?php while($new_loop->have_posts() ) :  $new_loop->the_post(); ?>
                    <div class="tile <?php the_field('tile_classes') ?>">
                        <div class="flipper">

                            <div class="<?php the_field('front_classes') ?>">

                                  <div class="<?php the_field('curve_classes'); ?>">

                                    <figure class="icon-wrapper <?php the_field('thumb_class'); ?>">
                                      
                                       <?php if ( has_post_thumbnail() ) : ?>
                                          <?php the_post_thumbnail('full', array('alt'=>get_the_title())); ?>
                                      <?php endif; ?>

                                    </figure>

                                    <h2 class="header-tile <?php the_field('front_header_class'); ?>">
                                      <span><?php the_field('header_top'); ?></span>
                                      <span><?php the_field('header_bottom'); ?></span>
                                    </h2>
                                  </div>
                            </div>



                            <div class="<?php the_field('back_classes'); ?>">

                                <div class="back-text-wrapper">
                                   <div class="back-text-inner">
                                     <h3 class="tile-back-header"><?php the_field('header_back'); ?></h3>

                                      <?php the_field('desc_back') ?>
                                   </div>
                                </div>
                            </div>

                        </div>
                    </div>
                <?php endwhile; ?>


            <?php endif; wp_reset_query(); ?>