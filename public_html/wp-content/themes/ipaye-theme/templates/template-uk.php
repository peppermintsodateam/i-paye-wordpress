
<?php if(have_rows('tabs_list') ) : ?>


<ul class="list-working-uk" id="list-working-uk">

<?php while(have_rows('tabs_list') ) : the_row(); ?>

  <li class="filter-category d-btn <?php the_sub_field('button_class'); ?>"><?php the_sub_field('list_item'); ?></li>

  <?php endwhile; ?>


    
     </ul>

     <?php endif; wp_reset_query(); ?>


<?php if(have_rows('uk_content') ) : ?>
     <section class="tabs-uk clearfix">
        <?php while(have_rows('uk_content') ) : the_row(); ?>

            <div class="tabs-content-uk">
                <?php the_sub_field('sections'); ?> 
            </div>

        <?php endwhile;?>
     </section>

     <?php endif; wp_reset_query(); ?>