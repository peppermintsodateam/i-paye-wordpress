<?php
/*
	Template Name: Page static Accountancy
*/
 ?>

<?php get_header(); ?>


    <body <?php body_class(); ?>>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

      <?php get_template_part('templates/form','panel');  ?>
    	<?php get_template_part('templates/navigation','green'); ?>


        <header class="home-header page-header">
           

            <div class="navigation-switcher">
                <div class="switcher-wrapper">
                   <div id="switcher">
                       <span class="slice slice1"></span>
                       <span class="slice slice2"></span>
                       <span class="slice slice3"></span>
                   </div>
                </div>
            </div>

            <?php get_template_part('templates/back','accountancy'); ?>
        </header>

        <section class="main-wrapper">

          <?php get_sidebar('green'); ?>

          <div class="page-container single-page">

                <div class="page-inner single-page"">
                      
                  <section class="page-content clearfix">

                        <section class="main-page-content">
                          <?php if(have_posts() ) : ?>
                            <?php while(have_posts() ) : the_post(); ?>

                              <?php the_content(); ?>



                            <?php endwhile; ?>
                          <?php endif; wp_reset_query(); ?>
                          
                          <?php get_template_part('templates/page','socials'); ?>
                        </section>

                        <section class="page-bottom-area">
<h4 class="content-title-sub orange-sub"><?php the_field('call_paragraph'); ?> <a target="_blank" class="panel-contact-number" href="<?php the_field('phone_link'); ?>"><?php the_field('phone_text'); ?></a> </h4>
                        </section>


                  </section>
                    
                </div>
          </div>


          
          <?php get_template_part('templates/footer','bottomacc'); ?>
            
        </section>

 
<?php get_footer(); ?>


    </body>
</html>
