<?php 
    $short_content = get_field('short_content');
?>

<aside class="left-panel-wrapper i-paye-left-panel">
            <div class="bck-top-purple"></div>

            <div class="left-content-scroll">
              <div class="panel-content">
                <div class="panel-content-inside">
                    <figure class="logo-icon-panel">
                        <img class="panel-logo" src="<?php echo THEME_PATH; ?>/gfx/i-paye-accountancy-logo.svg" alt="Accountancy Logo">
                    </figure>

                    <div class="content-panel-text">
                        <h1 class="main-title-panel-left green-header-left"><?php the_title(); ?></h1>
                        <?php echo $short_content; ?>
                    </div>

                    <div class="panel-contact-details green-details">
                        <?php if(is_active_sidebar('green-widget')) : ?>

                            <?php dynamic_sidebar('green-widget'); ?>

                        <?php endif; ?>
                    </div>

                    <?php get_template_part('templates/spanel','green'); ?>

                </div>
              </div>
            </div>

            <div class="bck-bottom"></div>
</aside>