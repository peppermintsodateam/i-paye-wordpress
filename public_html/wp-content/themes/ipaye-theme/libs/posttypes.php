<?php 
	add_action('init', 'ipaye_init_posttypes');

	function ipaye_init_posttypes() {



		/*Home slider*/
		$labels = array(
			'name'=>__('Home slider'),
			'singular_name'=>__('Home slider'),
			'add_new'=>__('Add new home slide'),
			'add_new_item'=>__('Add new home slide'),
			'edit_item'=>__('Edit home slide'),
			'new_item'=>__('New home slide'),
			'view_item'=>__('View home slide'),
			'search_items'=>__('Search home slide'),
			'not_found'=>__('No home slide found'),
			'not_found_in_trash' =>__('No home slide found in trash')
		);

	$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
		);

		register_post_type('home_slider', $args);


		/*Home slider*/
		$labels = array(
			'name'=>__('Social icons'),
			'singular_name'=>__('Social icons'),
			'add_new'=>__('Add new social icon'),
			'add_new_item'=>__('Add new social icon'),
			'edit_item'=>__('Edit social icon'),
			'new_item'=>__('New social icon'),
			'view_item'=>__('View social icon'),
			'search_items'=>__('Search social icon'),
			'not_found'=>__('No social icon found'),
			'not_found_in_trash' =>__('No social icon found in trash')
		);

	$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
		);

		register_post_type('social_icons', $args);



		/*Features I-paye*/
		$labels = array(
			'name'=>__('Features Ipaye'),
			'singular_name'=>__('Features Ipaye'),
			'add_new'=>__('Add new feature'),
			'add_new_item'=>__('Add new feature'),
			'edit_item'=>__('Edit feature'),
			'new_item'=>__('New feature'),
			'view_item'=>__('View feature'),
			'search_items'=>__('Search feature'),
			'not_found'=>__('No feature found'),
			'not_found_in_trash' =>__('No feature found in trash')
		);

	$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
		);

		register_post_type('features_ipaye', $args);


		/*Features Accountancy*/
		$labels = array(
			'name'=>__('Features Accountancy'),
			'singular_name'=>__('Features Accountancy'),
			'add_new'=>__('Add new feature'),
			'add_new_item'=>__('Add new feature'),
			'edit_item'=>__('Edit feature'),
			'new_item'=>__('New feature'),
			'view_item'=>__('View feature'),
			'search_items'=>__('Search feature'),
			'not_found'=>__('No feature found'),
			'not_found_in_trash' =>__('No feature found in trash')
		);

	$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
		);

		register_post_type('features_accountancy', $args);


		/*Features SPI*/
		$labels = array(
			'name'=>__('Features Spi'),
			'singular_name'=>__('Features Spi'),
			'add_new'=>__('Add new feature'),
			'add_new_item'=>__('Add new feature'),
			'edit_item'=>__('Edit feature'),
			'new_item'=>__('New feature'),
			'view_item'=>__('View feature'),
			'search_items'=>__('Search feature'),
			'not_found'=>__('No feature found'),
			'not_found_in_trash' =>__('No feature found in trash')
		);

	$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
		);

		register_post_type('features_spi', $args);


		/*Rotator features I-paye*/
		$labels = array(
			'name'=>__('Rotator features I-paye'),
			'singular_name'=>__('features I-paye'),
			'add_new'=>__('Add new slide'),
			'add_new_item'=>__('Add new slide'),
			'edit_item'=>__('Edit slide'),
			'new_item'=>__('New slide'),
			'view_item'=>__('View slide'),
			'search_items'=>__('Search slide'),
			'not_found'=>__('No slide found'),
			'not_found_in_trash' =>__('No slide found in trash')
		);

	$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
		);

		register_post_type('slides_fipaye', $args);


		/*Rotator features Accountancy*/
		$labels = array(
			'name'=>__('Rotator features accountancy'),
			'singular_name'=>__('features accountancy'),
			'add_new'=>__('Add new slide'),
			'add_new_item'=>__('Add new slide'),
			'edit_item'=>__('Edit slide'),
			'new_item'=>__('New slide'),
			'view_item'=>__('View slide'),
			'search_items'=>__('Search slide'),
			'not_found'=>__('No slide found'),
			'not_found_in_trash' =>__('No slide found in trash')
		);

	$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
		);

		register_post_type('slides_faccountancy', $args);


		/*Rotator features SPI*/
		$labels = array(
			'name'=>__('Rotator features spi'),
			'singular_name'=>__('features spi'),
			'add_new'=>__('Add new slide'),
			'add_new_item'=>__('Add new slide'),
			'edit_item'=>__('Edit slide'),
			'new_item'=>__('New slide'),
			'view_item'=>__('View slide'),
			'search_items'=>__('Search slide'),
			'not_found'=>__('No slide found'),
			'not_found_in_trash' =>__('No slide found in trash')
		);

	$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
		);

		register_post_type('slides_fspi', $args);



		/*Footer bottom logos set*/
		$labels = array(
			'name'=>__('Footer logo Ipaye'),
			'singular_name'=>__('footer logo'),
			'add_new'=>__('Add new footer logo'),
			'add_new_item'=>__('Add new footer logo'),
			'edit_item'=>__('Edit footer logo'),
			'new_item'=>__('New footer logo'),
			'view_item'=>__('View footer logo'),
			'search_items'=>__('Search footer logo'),
			'not_found'=>__('No footer logo found'),
			'not_found_in_trash' =>__('No footer logo found in trash')
		);

	$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
		);

		register_post_type('footer_logo', $args);


	/*Footer bottom accountancy set*/
		$labels = array(
			'name'=>__('Footer logo acco'),
			'singular_name'=>__('footer logo'),
			'add_new'=>__('Add new footer logo'),
			'add_new_item'=>__('Add new footer logo'),
			'edit_item'=>__('Edit footer logo'),
			'new_item'=>__('New footer logo'),
			'view_item'=>__('View footer logo'),
			'search_items'=>__('Search footer logo'),
			'not_found'=>__('No footer logo found'),
			'not_found_in_trash' =>__('No footer logo found in trash')
		);

	$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
		);

		register_post_type('footer_accountancy', $args);


		/*Footer bottom spi set*/
		$labels = array(
			'name'=>__('Footer logo spi'),
			'singular_name'=>__('footer logo'),
			'add_new'=>__('Add new footer logo'),
			'add_new_item'=>__('Add new footer logo'),
			'edit_item'=>__('Edit footer logo'),
			'new_item'=>__('New footer logo'),
			'view_item'=>__('View footer logo'),
			'search_items'=>__('Search footer logo'),
			'not_found'=>__('No footer logo found'),
			'not_found_in_trash' =>__('No footer logo found in trash')
		);

	$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
		);

		register_post_type('footer_spi', $args);



		/*Testimonials for Ipaye*/
		$labels = array(
			'name'=>__('Testimonials Ipaye'),
			'singular_name'=>__('Testimonials Ipaye'),
			'add_new'=>__('Add new testimonial'),
			'add_new_item'=>__('Add new testimonials'),
			'edit_item'=>__('Edit testimonials'),
			'new_item'=>__('New testimonials'),
			'view_item'=>__('View testimonials'),
			'search_items'=>__('Search testimonials'),
			'not_found'=>__('No testimonials found'),
			'not_found_in_trash' =>__('No testimonials found in trash')
		);

	$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
		);

		register_post_type('testimonials_ipaye', $args);

		/*Testimonials for Accountancy*/
		$labels = array(
			'name'=>__('Testimonials accountancy'),
			'singular_name'=>__('Testimonials accountancy'),
			'add_new'=>__('Add new testimonial'),
			'add_new_item'=>__('Add new testimonials'),
			'edit_item'=>__('Edit testimonials'),
			'new_item'=>__('New testimonials'),
			'view_item'=>__('View testimonials'),
			'search_items'=>__('Search testimonials'),
			'not_found'=>__('No testimonials found'),
			'not_found_in_trash' =>__('No testimonials found in trash')
		);

	$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
		);

		register_post_type('testimonials_acco', $args);


		/*Testimonials for Spi*/
		$labels = array(
			'name'=>__('Testimonials spi'),
			'singular_name'=>__('Testimonials spi'),
			'add_new'=>__('Add new testimonial'),
			'add_new_item'=>__('Add new testimonials'),
			'edit_item'=>__('Edit testimonials'),
			'new_item'=>__('New testimonials'),
			'view_item'=>__('View testimonials'),
			'search_items'=>__('Search testimonials'),
			'not_found'=>__('No testimonials found'),
			'not_found_in_trash' =>__('No testimonials found in trash')
		);

	$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
		);

		register_post_type('testimonials_for_spi', $args);


		/*Meet the team*/
		$labels = array(
			'name'=>__('Team'),
			'singular_name'=>__('team'),
			'add_new'=>__('Add new team'),
			'add_new_item'=>__('Add new team'),
			'edit_item'=>__('Edit team'),
			'new_item'=>__('New team'),
			'view_item'=>__('View team'),
			'search_items'=>__('Search team'),
			'not_found'=>__('No team found'),
			'not_found_in_trash' =>__('No team found in trash')
		);

	$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
		);

		register_post_type('team', $args);

		/*REST OF THE TEAM PAGES MAYBE RESTORE LATER*/
		
		/*$labels = array(
			'name'=>__('Team acc'),
			'singular_name'=>__('team acc'),
			'add_new'=>__('Add new team acc'),
			'add_new_item'=>__('Add new team acc'),
			'edit_item'=>__('Edit team acc'),
			'new_item'=>__('New team acc'),
			'view_item'=>__('View team acc'),
			'search_items'=>__('Search team acc'),
			'not_found'=>__('No team acc found'),
			'not_found_in_trash' =>__('No team acc found in trash')
		);

	$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
		);

		register_post_type('team_acc', $args);



		
		$labels = array(
			'name'=>__('Team spi'),
			'singular_name'=>__('team spi'),
			'add_new'=>__('Add new team spi'),
			'add_new_item'=>__('Add new team spi'),
			'edit_item'=>__('Edit team spi'),
			'new_item'=>__('New team spi'),
			'view_item'=>__('View team spi'),
			'search_items'=>__('Search team spi'),
			'not_found'=>__('No team spi found'),
			'not_found_in_trash' =>__('No team spi found in trash')
		);

	$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
		);

		register_post_type('team_spi', $args);*/



	/*Case studies I-paye*/
		$labels = array(
			'name'=>__('Cases Ipaye'),
			'singular_name'=>__('Case Ipaye'),
			'add_new'=>__('Add new Case Ipaye'),
			'add_new_item'=>__('Add new Case Ipaye'),
			'edit_item'=>__('Edit Case Ipaye'),
			'new_item'=>__('New Case Ipaye'),
			'view_item'=>__('View Case Ipaye'),
			'search_items'=>__('Search Case Ipaye'),
			'not_found'=>__('No Case Ipaye found'),
			'not_found_in_trash' =>__('No Case Ipaye found in trash')
		);

	$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
		);

		register_post_type('cases_ipaye', $args);


		/*Case studies Accountancy*/
		$labels = array(
			'name'=>__('Cases Accountancy'),
			'singular_name'=>__('Case Accountancy'),
			'add_new'=>__('Add new Case Accountancy'),
			'add_new_item'=>__('Add new Case Accountancy'),
			'edit_item'=>__('Edit Case Accountancy'),
			'new_item'=>__('New Case Accountancy'),
			'view_item'=>__('View Case Accountancy'),
			'search_items'=>__('Search Case Accountancy'),
			'not_found'=>__('No Case Accountancyfound'),
			'not_found_in_trash' =>__('No Case Accountancy found in trash')
		);

	$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
		);

		register_post_type('cases_accountancy', $args);


	/*Case studies Spi*/
		$labels = array(
			'name'=>__('Cases Spi'),
			'singular_name'=>__('Cases Spi'),
			'add_new'=>__('Add new Cases Spi'),
			'add_new_item'=>__('Add new Cases Spi'),
			'edit_item'=>__('Edit Cases Spi'),
			'new_item'=>__('New Cases Spi'),
			'view_item'=>__('View Cases Spi'),
			'search_items'=>__('Search Cases Spi'),
			'not_found'=>__('No Cases Spi'),
			'not_found_in_trash' =>__('No Cases Spi found in trash')
		);

	$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
		);

		register_post_type('cases_spi', $args);


		/*Videos */
		$labels = array(
			'name'=>__('Videos ipaye'),
			'singular_name'=>__('Video ipaye'),
			'add_new'=>__('Add new videos ipaye'),
			'add_new_item'=>__('Add new videos ipaye'),
			'edit_item'=>__('Edit videos ipaye'),
			'new_item'=>__('New videos ipaye'),
			'view_item'=>__('View videos ipaye'),
			'search_items'=>__('Search videos ipaye'),
			'not_found'=>__('No videos ipaye'),
			'not_found_in_trash' =>__('No videos ipaye found in trash')
		);

	$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
		);

		register_post_type('videos_ipaye', $args);



	/*Accreditations logo set */
		$labels = array(
			'name'=>__('Accreditations logo'),
			'singular_name'=>__('Accreditations logo'),
			'add_new'=>__('Add new Accreditations logo'),
			'add_new_item'=>__('Add new Accreditations logo'),
			'edit_item'=>__('Edit Accreditations logo'),
			'new_item'=>__('New Accreditations logo'),
			'view_item'=>__('View Accreditations logo'),
			'search_items'=>__('Search Accreditations logo'),
			'not_found'=>__('No Accreditations logo'),
			'not_found_in_trash' =>__('No Accreditations logo found in trash')
		);

	$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
		);

		register_post_type('accredit_logo', $args);


		/*Accreditation accountancy logo*/
		$labels = array(
			'name'=>__('Accred acc logo'),
			'singular_name'=>__('Accred acc logo'),
			'add_new'=>__('Add new Accred acc logo'),
			'add_new_item'=>__('Add new Accred acc logo'),
			'edit_item'=>__('Edit Accred acc logo'),
			'new_item'=>__('New Accred acc logo'),
			'view_item'=>__('View Accred acc logo'),
			'search_items'=>__('Search Accred acc logo'),
			'not_found'=>__('No Accred acc logo'),
			'not_found_in_trash' =>__('No Accred acc logo found in trash')
		);

	$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
		);

		register_post_type('accred_acc_logo', $args);


		/*Accreditation spi logo*/
		$labels = array(
			'name'=>__('Accred spi logo'),
			'singular_name'=>__('Accred spi logo'),
			'add_new'=>__('Add new Accred spi logo'),
			'add_new_item'=>__('Add new Accred spi logo'),
			'edit_item'=>__('Edit Accred spi logo'),
			'new_item'=>__('New Accred spi logo'),
			'view_item'=>__('View Accred spi logo'),
			'search_items'=>__('Search Accred spi logo'),
			'not_found'=>__('No Accred spi logo'),
			'not_found_in_trash' =>__('No Accred spi logo found in trash')
		);

	$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
		);

		register_post_type('accred_spi_logo', $args);


		/*I paye rotator */
		$labels = array(
			'name'=>__('Ipaye rotator'),
			'singular_name'=>__('Ipaye rotator'),
			'add_new'=>__('Add new slide'),
			'add_new_item'=>__('Add new slide'),
			'edit_item'=>__('Edit slide'),
			'new_item'=>__('New slide'),
			'view_item'=>__('View slide'),
			'search_items'=>__('Search slide'),
			'not_found'=>__('No slide'),
			'not_found_in_trash' =>__('No slide found in trash')
		);

	$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
		);

		register_post_type('ipaye_rotator', $args);


		/*Accountancy rotator */
		$labels = array(
			'name'=>__('Accountancy rotator'),
			'singular_name'=>__('Accountancy rotator'),
			'add_new'=>__('Add new slide'),
			'add_new_item'=>__('Add new slide'),
			'edit_item'=>__('Edit slide'),
			'new_item'=>__('New slide'),
			'view_item'=>__('View slide'),
			'search_items'=>__('Search slide'),
			'not_found'=>__('No slide'),
			'not_found_in_trash' =>__('No slide found in trash')
		);

	$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
		);

		register_post_type('accountancy_rotator', $args);


		/*SPi rotator */
		$labels = array(
			'name'=>__('SPi rotator'),
			'singular_name'=>__('SPi rotator'),
			'add_new'=>__('Add new slide'),
			'add_new_item'=>__('Add new slide'),
			'edit_item'=>__('Edit slide'),
			'new_item'=>__('New slide'),
			'view_item'=>__('View slide'),
			'search_items'=>__('Search slide'),
			'not_found'=>__('No slide'),
			'not_found_in_trash' =>__('No slide found in trash')
		);

	$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
		);

		register_post_type('spi_rotator', $args);



	}
?>